import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './login.component';
import {LoginRoutingModule} from './loginRouting.module';
import {MatFormFieldModule, MatInputModule, MatSnackBarModule} from '@angular/material';
import {ReactiveFormsModule} from '@angular/forms';
import {ButtonModule} from '../../shared/modules/button/button.module';
import {LoaderModule} from '../../shared/modules/loader/loader.module';
import {AutoFocusModule} from '../../directives/auto-focus.module';
import {SnackBarPreviewComponent} from '../../shared/modules/snack-bar-preview/snack-bar-preview.component';
import {SnackBarPreviewModule} from '../../shared/modules/snack-bar-preview/snack-bar-preview.module';

@NgModule({
    declarations: [LoginComponent],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        LoginRoutingModule,
        MatFormFieldModule,
        MatInputModule,
        MatSnackBarModule,
        ButtonModule,
        LoaderModule,
        AutoFocusModule,
        SnackBarPreviewModule,
    ],
    entryComponents: [SnackBarPreviewComponent],
})
export class LoginModule {}
