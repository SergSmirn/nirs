export type ExperimentStatusType = 'finished' | 'calculating' | 'started' | 'in_queue';

export enum ExperimentStatus {
    finished = 'finished',
    calculating = 'calculating',
    started = 'started',
    inQueue = 'in_queue',
}

export interface IExperimentStatus {
    status: ExperimentStatusType;
    progress?: number;
    id?: number;
}

export interface IExperiment extends IExperimentStatus {
    id?: number;
    createdExperiment?: number;

    device: IDevice;

    modelType: string;
    simulation: string;
    geometry: string;
    experimentalData: any;

    // psyh
    diffCoeff: number;
    ambipolarDiffCoeff: number;
}

export interface IResultExperiment extends IExperiment {
    trialsCount: number;
    particlesCount: number;
    letValuesCount: number;
    simulationResult: any;
    calculatingTime: number;
    ser: number;
    par1: number;
    par2: number;
    spectre: any;
    spectreFile: string;
    experimentalDataFile: string;
    spectreFileText: string;
}

export interface IDevice {
    id?: number;
    name: string;
    processNode?: number;
    voltage?: number;
    resistance?: number;
    capacitance?: number;
    producer?: string;
    technology?: string;
    comment: string;
}
