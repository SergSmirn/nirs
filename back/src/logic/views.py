from rest_framework import generics
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.permissions import IsAuthenticated
from logic.models import Device, Experiment
from logic.serializers import ExperimentBaseAccessSerializer, ExperimentDetailSerializer, DeviceSerializer, \
    ExperimentFullAccessSerializer
from logic.task import run_calc


class ExperimentListView(generics.ListCreateAPIView):
    permission_classes = (IsAuthenticated,)
    parser_classes = (FormParser, MultiPartParser)

    def get_serializer_class(self):
        if self.request.user.groups.filter(name='base_access').exists():
            return ExperimentBaseAccessSerializer
        elif self.request.user.groups.filter(name='full_access').exists():
            return ExperimentFullAccessSerializer

    def get_queryset(self):
        return Experiment.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        instance = serializer.save()
        run_calc(instance.pk, self.request.user.pk)


class DeviceListView(generics.ListCreateAPIView):
    serializer_class = DeviceSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Device.objects.filter(user=self.request.user)


class DeviceUpdateView(generics.UpdateAPIView):
    serializer_class = DeviceSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Device.objects.filter(user=self.request.user)


class ExperimentDetailView(generics.RetrieveDestroyAPIView):
    serializer_class = ExperimentDetailSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Experiment.objects.filter(user=self.request.user)
