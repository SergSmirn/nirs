import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from '../models/user.model';

@Injectable({
    providedIn: 'root',
})
export class UserStateService {
    private readonly _user = new BehaviorSubject<User>(null);

    get user$(): Observable<User> {
        return this._user.asObservable();
    }

    get user(): User | null {
        return this._user.value;
    }

    set user(user: User | null) {
        this._user.next(user);
    }
}
