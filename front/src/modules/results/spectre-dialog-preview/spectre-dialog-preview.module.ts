import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SpectreDialogPreviewComponent} from './spectre-dialog-preview.component';
import {MatTableModule} from '@angular/material';
import {DimensionModule} from '../../../shared/modules/dimension/dimension.module';

@NgModule({
    declarations: [SpectreDialogPreviewComponent],
    imports: [CommonModule, MatTableModule, DimensionModule],
})
export class SpectreDialogPreviewModule {}
