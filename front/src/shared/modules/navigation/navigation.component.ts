import {ChangeDetectionStrategy, Component} from '@angular/core';
import {UserStateService} from '../../../services/user-state.service';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {AuthService} from '../../../services/auth.service';

@Component({
    selector: 'app-navigation',
    templateUrl: './navigation.component.html',
    styleUrls: ['./navigation.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NavigationComponent {
    isLogin$: Observable<boolean>;

    constructor(
        private readonly userStateService: UserStateService,
        private readonly authService: AuthService,
    ) {
        this.isLogin$ = this.userStateService.user$.pipe(
            map(user => user && user.isAuth),
        );
    }

    logout() {
        this.authService.logout().subscribe();
    }
}
