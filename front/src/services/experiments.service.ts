import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {UserStateService} from './user-state.service';
import {convertFromDTO, convertToDTO} from '../utils/convertDTO';
import {IDevice, IExperiment, IResultExperiment} from '../models/experiments.model';
import Centrifuge from 'centrifuge';
import {environment} from '../environments/environment';

const host = environment.production ? '185.58.207.11' : 'localhost:8002';
const CENTRIFUGO_URL = `ws://${host}/connection/websocket`;

@Injectable({
    providedIn: 'root',
})
export class ExperimentsService {
    _experiments: IExperiment[] = [];
    readonly _experiments$ = new BehaviorSubject<IExperiment[]>(null);

    readonly cachedExperiments = new Map<number, IResultExperiment>();

    private socket: any;

    private _devices: IDevice[] = [];
    private readonly _devices$ = new BehaviorSubject<IDevice[]>(null);

    private readonly socketInformation$ = new Subject<IExperiment[]>();

    constructor(
        private readonly http: HttpClient,
        private readonly userStateService: UserStateService,
        private readonly router: Router,
    ) {}

    get socketInfo$(): Observable<any> {
        return this.socketInformation$.asObservable();
    }

    get experiments$(): Observable<IExperiment[]> {
        return this._experiments$.asObservable();
    }

    get devices$(): Observable<IDevice[]> {
        return this._devices$.asObservable();
    }

    getExperimentResult(id: number, hardReload: boolean = false) {
        const cachedExperiments = this.cachedExperiments.get(id);

        if (cachedExperiments && !hardReload) {
            return;
        }

        const url = `/api/experiment/${id}/`;

        this.http
            .get<IResultExperiment>(url, {withCredentials: true})
            .pipe(
                map(convertFromDTO),
                tap(experiment => this.cachedExperiments.set(id, experiment)),
            )
            .subscribe(
                resultExperiment => {
                    const findExp = this._experiments.find(experiment => {
                        return experiment.id === resultExperiment.id;
                    });

                    if (!findExp) {
                        this._experiments.push(resultExperiment);
                    }

                    this._experiments = this._experiments.map(experiment => {
                        return experiment.id === resultExperiment.id
                            ? {...experiment, ...resultExperiment}
                            : experiment;
                    });

                    this._experiments$.next(this._experiments);
                },
                () => {
                    this.router.navigate([], {queryParams: []});
                },
            );
    }

    updateExperimentsStorage() {
        this.http
            .get<IExperiment[]>('/api/experiment/', {withCredentials: true})
            .pipe(
                map(convertFromDTO),
                tap((experiments: IExperiment[]) => {
                    this._experiments = experiments.map(experiment => {
                        return {
                            ...this.cachedExperiments.get(experiment.id),
                            ...experiment,
                        };
                    });
                    this._experiments$.next(this._experiments);
                }),
            )
            .subscribe();
    }

    deleteExperiment(id: number): Observable<void> {
        const url = `api/experiment/${id}/`;

        return this.http.delete<void>(url, {withCredentials: true}).pipe(
            tap(() => {
                this._experiments = this._experiments.filter(experiment => {
                    return experiment.id !== id;
                });
                this._experiments$.next(this._experiments);
            }),
        );
    }

    updateDevicesStorage() {
        this.http
            .get<IDevice[]>('/api/device/', {withCredentials: true})
            .pipe(
                map(convertFromDTO),
                tap((devices: IDevice[]) => {
                    this._devices = devices;
                    this._devices$.next(this._devices);
                }),
            )
            .subscribe();
    }

    newDevice(device: IDevice): Observable<IDevice> {
        const deviceToDto = convertToDTO(device);

        return this.http
            .post<IDevice>('/api/device/', deviceToDto, {withCredentials: true})
            .pipe(
                map(convertFromDTO),
                tap((newDevice: IDevice) => {
                    this._devices.push(newDevice);
                    this._devices$.next(this._devices);
                }),
            );
    }

    newExperiment(data: FormData): Observable<IExperiment> {
        return this.http
            .post<IExperiment>('/api/experiment/', data, {withCredentials: true})
            .pipe(
                map(convertFromDTO),
                tap(experiment => {
                    this._experiments = [...this._experiments, experiment];
                    this._experiments$.next(this._experiments);
                }),
            );
    }

    centrifugo() {
        if (this.socket) {
            return;
        }

        const user = this.userStateService.user;

        if (!user.isAuth) {
            return;
        }

        const centrifuge = new Centrifuge(CENTRIFUGO_URL);

        this.socket = centrifuge;

        centrifuge.setToken(user.info.token);
        centrifuge.subscribe(`#${user.info.id}`, ({data}) => {
            this.socketInformation$.next(convertFromDTO(data));
        });
        centrifuge.connect();
    }
}
