import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatSelectModule,
    MatSlideToggleModule,
    MatSnackBarModule,
} from '@angular/material';
import {ExperimentComponent} from './experiment.component';
import {ExperimentRoutingModule} from './experimentRouting.module';
import {ButtonModule} from '../../shared/modules/button/button.module';
import {LoaderModule} from '../../shared/modules/loader/loader.module';
import {InputFileModule} from '../../shared/modules/input-file/input-file.module';
import {DevicePreviewPipe} from '../../pipes/device-preview.pipe';

@NgModule({
    declarations: [ExperimentComponent, DevicePreviewPipe],
    imports: [
        CommonModule,
        ExperimentRoutingModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatSnackBarModule,
        MatSlideToggleModule,
        MatIconModule,
        ButtonModule,
        LoaderModule,
        InputFileModule,
    ],
    entryComponents: [],
})
export class ExperimentModule {}
