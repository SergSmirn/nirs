from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from accounts import views as accounts_views
from logic import views as logic_views

urlpatterns = [
    path('admin/', admin.site.urls),

    # user
    path('api/signup/', accounts_views.signup),
    path('api/login/', accounts_views.signin),
    path('api/logout/', accounts_views.signout),
    path('api/init/', accounts_views.init),
    path('api/activate/<uidb64>/<token>/', accounts_views.activate, name='activate'),

    # logic
    path('api/experiment/', logic_views.ExperimentListView.as_view()),
    path('api/experiment/<pk>/', logic_views.ExperimentDetailView.as_view()),
    path('api/device/', logic_views.DeviceListView.as_view()),
    path('api/device/<pk>', logic_views.DeviceUpdateView.as_view()),

    ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
