export interface IUser {
    username: string;
    password: string;
    email?: string;
    token: string;
    id?: string;
    group: string;
    hasExperiments: boolean;
}

export enum GroupType {
    base,
    full,
}

export class User {
    constructor(private readonly userData: IUser, private readonly _isAuth: boolean) {}

    get isAuth(): boolean {
        return this._isAuth;
    }

    get info(): IUser | null {
        return this.isAuth ? this.userData : null;
    }

    get group(): GroupType | null {
        const info = this.info;

        return info
            ? info.group === 'base_access'
                ? GroupType.base
                : GroupType.full
            : null;
    }
}

export interface IUserFromDto {
    isAuthenticated: boolean;
    userData: IUser;
}
