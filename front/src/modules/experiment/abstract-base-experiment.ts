import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ValidatorFn} from '@angular/forms/src/directives/validators';

export const deviceBaseControl: IControls = {
    device: {
        value: null,
        validators: [Validators.required],
    },
};

export const experimentBaseControls: IControls = {
    ...deviceBaseControl,
    experimentalData: {
        value: null,
        validators: [Validators.required],
    },
    spectre: {
        value: null,
        validators: [Validators.required],
    },
};

export const deviceBaseControls: IControls = {
    name: {
        value: null,
        validators: [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(30),
        ],
    },
    manufacturer: {
        value: null,
        validators: [Validators.minLength(2), Validators.maxLength(30)],
    },
    technology: {
        value: null,
        validators: [Validators.minLength(2), Validators.maxLength(30)],
    },
    comment: {
        value: null,
        validators: [Validators.minLength(2), Validators.maxLength(30)],
    },
};

interface IBaseExperiment extends IBaseDevice {
    readonly device: FormControl;
    readonly experimentalData: FormControl;
    readonly spectre: FormControl;
}

interface IBaseDevice {
    readonly name: FormControl;
    readonly manufacturer: FormControl;
    readonly technology: FormControl;
    readonly comment: FormControl;
}

export interface IControls {
    [key: string]: {
        value: any;
        validators: ValidatorFn | ValidatorFn[] | null;
    };
}

export abstract class AbstractBaseExperiment implements IBaseExperiment {
    protected form = new FormGroup({});

    constructor() {
        this.setControls(experimentBaseControls);
        // this.setControls(deviceBaseControls);
    }

    get device(): FormControl | null {
        return <FormControl>this.form.get('device') || null;
    }

    get experimentalData(): FormControl | null {
        return <FormControl>this.form.get('experimentalData') || null;
    }

    get spectre(): FormControl {
        return <FormControl>this.form.get('spectre');
    }

    get newDevice(): FormGroup | null {
        return <FormGroup>this.form.get('newDevice') || null;
    }

    // device
    get name(): FormControl | null {
        return <FormControl>this.newDevice.get('name') || null;
    }

    get manufacturer(): FormControl | null {
        return <FormControl>this.newDevice.get('manufacturer') || null;
    }

    get technology(): FormControl | null {
        return <FormControl>this.newDevice.get('technology') || null;
    }

    get comment(): FormControl | null {
        return <FormControl>this.newDevice.get('comment') || null;
    }

    protected getControlsByConfig(
        controlsObjects: IControls,
    ): {[key: string]: FormControl} {
        const controls = {};

        Object.keys(controlsObjects).forEach(controlName => {
            const control = new FormControl(controlsObjects[controlName].value);

            control.setValidators(controlsObjects[controlName].validators);

            controls[controlName] = control;
        });

        return controls;
    }

    protected setControls(controlsObjects: IControls) {
        Object.keys(controlsObjects).forEach(controlName => {
            const control = new FormControl(controlsObjects[controlName].value);

            control.setValidators(controlsObjects[controlName].validators);

            this.form.addControl(controlName, control);
        });
    }

    protected markAsTouched() {
        Object.keys(experimentBaseControls).forEach(controlKey => {
            const control = this.form.get(controlKey);

            if (control) {
                control.markAsTouched();
            }
        });

        if (!this.newDevice) {
            return;
        }

        Object.keys(deviceBaseControls).forEach(controlKey => {
            const control = this.newDevice.get(controlKey);

            if (control) {
                control.markAsTouched();
            }
        });
    }

    protected getToDtoExperiment(formData: FormData): FormData {
        formData.append('spectre', this.spectre.value);

        if (this.device) {
            formData.append('device', this.device.value);
        }

        return formData;
    }
}
