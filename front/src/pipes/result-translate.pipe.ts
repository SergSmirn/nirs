import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'resultTranslate',
})
export class ResultTranslatePipe implements PipeTransform {
    resultVariants = {
        point: 'Точечная',
        diffusion: 'Диффузионная',
        analytical: 'Аналитический',
        monte_carlo: 'Монте-Карло',
        disc: 'Диск',
        sphere: 'Сфера',
    };

    transform(value: string): any {
        const translate = this.resultVariants[value];

        return !!translate ? translate : value;
    }
}
