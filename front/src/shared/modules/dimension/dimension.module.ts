import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DimensionComponent} from './dimension.component';

@NgModule({
    declarations: [DimensionComponent],
    imports: [CommonModule],
    exports: [DimensionComponent],
})
export class DimensionModule {}
