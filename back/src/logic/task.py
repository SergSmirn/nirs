from multiprocessing import freeze_support
from logic.models import Experiment
from logic import logic
from nirs.celery import app
import numpy as np
from cent import Client
import datetime


url = "http://centrifugo:8000"
secret_key = "secret"


@app.task
def run_calc(exp_id, user_pk):
    exp = Experiment.objects.get(pk=exp_id)
    client = Client(url, secret_key, timeout=1)
    exp.started_at = datetime.datetime.now()
    exp.save()
    client.publish('#{}'.format(user_pk), {'status': 'started', 'exp_id': exp_id})
    freeze_support()
    params, cross_section = logic.cross_section_fit(exp)
    exp.cross_section = cross_section
    exp.par1, exp.par2 = params
    print('Found par1 = {0}, par2 = {1}'.format(exp.par1, exp.par2))
    if exp.sim_type == 'monte_carlo':
        exp.simulation_result = logic.run_monte_carlo(exp, client)
    elif exp.sim_type == 'analytical':
        exp.simulation_result = logic.run_analytical(exp)
    # print(exp.simulation_result)

    # fig, ax1 = plt.subplots()
    # plt.gca().set_xscale('log')
    # ax1.plot(exp.simulation_result[0], exp.simulation_result[1], 'bo')
    # plt.show()

    # calculate SER value
    ser = logic.calculate_ser(exp.simulation_result, exp.spectre)
    print("SER = {0}".format(ser))
    exp.ser = ser
    exp.finished_at = datetime.datetime.now()
    exp.save()
    client.publish('#{}'.format(user_pk), {'status': 'finished', 'exp_id': exp_id})
