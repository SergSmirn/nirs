import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    Inject,
} from '@angular/core';
import {MAT_DIALOG_DATA, MatTableDataSource} from '@angular/material';
import {
    DimensionSigns,
    IDimension,
} from '../../../shared/modules/dimension/dimension.component';

interface ISpectreElements {
    Number: number;
    lpe: number;
    section: number;
}

@Component({
    selector: 'app-spectre-dialog-preview',
    templateUrl: './spectre-dialog-preview.component.html',
    styleUrls: ['./spectre-dialog-preview.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SpectreDialogPreviewComponent {
    displayedColumns: string[] = ['Number', 'lpe', 'section'];

    tableData: ISpectreElements[] = [{Number: 1, lpe: 1, section: 2}];

    dataSource: any;

    readonly lpeDimension: IDimension[] = [
        {
            dimension: 'МэВ',
        },
        {
            dimension: 'см',
            power: 2,
        },
        {
            dimension: 'мг',
            sign: DimensionSigns.division,
        },
    ];

    readonly sectionDimension: IDimension[] = [
        {
            dimension: 'см',
            power: 2,
        },
    ];

    constructor(
        private readonly cd: ChangeDetectorRef,
        @Inject(MAT_DIALOG_DATA) private data: number[][],
    ) {
        const lpeData = data[0];
        const sectionData = data[1];

        this.tableData = lpeData.map((lpe, i) => {
            return {lpe, section: sectionData[i], Number: i + 1};
        });

        this.dataSource = new MatTableDataSource<ISpectreElements>(this.tableData);
    }
}
