import {convertFromDTO, convertToDTO} from '../convertDTO';

describe('covertDto', () => {
    describe('convertToDTO', () => {
        it('объект пустой, результат пустой', () => {
            const obj = {};
            const converted = convertToDTO(obj);

            expect(converted).toEqual({});
        });

        it('изменяет ключ на dto ключ', () => {
            const obj = {modelType: 'someValue'};
            const converted = convertToDTO(obj);

            expect(converted).toEqual({model_type: 'someValue'});
        });

        it('Не мутирует значения по ключу', () => {
            const obj = {
                experiment: 2000,
                someKey: 213,
                converted: 'test - value',
            };
            const converted = convertToDTO(obj);

            expect(converted.experiment).toBe(2000);
            expect(converted.someKey).toBe(213);
            expect(converted.converted).toBe('test - value');
        });

        it('Тест на все ключи', () => {
            const obj = {
                modelType: 'model_type',
                simulation: 'sim_type',
                experimentalData: 'experimental_data',
                processNode: 'process_node',
                voltage: 'supply_voltage',
                diffCoeff: 'diff_coefficient',
                ambipolarDiffCoeff: 'ambipolar_diff_coefficient',
                hasExperiments: 'has_running_experiments',
            };
            const expectObj = {
                model_type: 'model_type',
                sim_type: 'sim_type',
                experimental_data: 'experimental_data',
                process_node: 'process_node',
                supply_voltage: 'supply_voltage',
                diff_coefficient: 'diff_coefficient',
                ambipolar_diff_coefficient: 'ambipolar_diff_coefficient',
                has_running_experiments: 'has_running_experiments',
            };
            const converted = convertToDTO(obj);

            expect(converted).toEqual(expectObj);
            expect(Object.keys(obj).length).toBe(Object.keys(expectObj).length);
        });

        it('Тест на Юзера', () => {
            const obj = {
                hasExperiments: 'has_running_experiments',
                avatar: 'user_avatar',
                lastName: 'last_name',
                firstName: 'first_name',
            };
            const expObj = {
                has_running_experiments: 'has_running_experiments',
                user_avatar: 'user_avatar',
                last_name: 'last_name',
                first_name: 'first_name',
            };
            const converted = convertToDTO(obj);

            expect(converted).toEqual(expObj);
            expect(Object.keys(converted).length).toBe(Object.keys(expObj).length);
        });

        it('Тест на socketDTO', () => {
            const obj = {
                experimentId: 'exp_id',
            };
            const expObj = {
                exp_id: 'exp_id',
            };
            const converted = convertToDTO(obj);

            expect(converted).toEqual(expObj);
            expect(Object.keys(converted).length).toBe(Object.keys(expObj).length);
        });

        it('Тест на deviceDTO', () => {
            const obj = {
                processNode: 'process_node',
                voltage: 'supply_voltage',
            };
            const expObj = {
                process_node: 'process_node',
                supply_voltage: 'supply_voltage',
            };
            const converted = convertToDTO(obj);

            expect(converted).toEqual(expObj);
            expect(Object.keys(converted).length).toBe(Object.keys(expObj).length);
        });

        it('Тест на ResultExperimentDTO', () => {
            const obj = {
                trialsCount: 'trials_count',
                particlesCount: 'particles_count',
                letValuesCount: 'let_values_count',
                simulationResult: 'simulation_result',
                experimentalDataFile: 'data_file_name',
                spectreFile: 'spectre_file_name',
            };
            const expObj = {
                trials_count: 'trials_count',
                particles_count: 'particles_count',
                let_values_count: 'let_values_count',
                simulation_result: 'simulation_result',
                data_file_name: 'data_file_name',
                spectre_file_name: 'spectre_file_name',
            };
            const converted = convertToDTO(obj);

            expect(converted).toEqual(expObj);
            expect(Object.keys(converted).length).toBe(Object.keys(expObj).length);
        });

        it('Тест на experimentDTO', () => {
            const obj = {
                createdExperiment: 'created_at',
                modelType: 'model_type',
                simulation: 'sim_type',
                experimentalData: 'experimental_data',
                diffCoeff: 'diff_coefficient',
                ambipolarDiffCoeff: 'ambipolar_diff_coefficient',
                calculatingTime: 'calculating_duration',
            };
            const expObj = {
                created_at: 'created_at',
                model_type: 'model_type',
                sim_type: 'sim_type',
                experimental_data: 'experimental_data',
                diff_coefficient: 'diff_coefficient',
                ambipolar_diff_coefficient: 'ambipolar_diff_coefficient',
                calculating_duration: 'calculating_duration',
            };
            const converted = convertToDTO(obj);

            expect(converted).toEqual(expObj);
            expect(Object.keys(converted).length).toBe(Object.keys(expObj).length);
        });
    });

    describe('convertFromDTO', () => {
        it('Значение пустое - результат пустой', () => {
            const obj = {};
            const converted = convertFromDTO(obj);

            expect(converted).toEqual({});
        });

        it('изменяет dto ключ на interface', () => {
            const obj = {model_type: 'someValue'};
            const converted = convertFromDTO(obj);

            expect(converted).toEqual({modelType: 'someValue'});
        });

        it('изменяет ключ на dto ключ, не трогает тех, что нет в конвертации', () => {
            const obj = {
                model_type: 'someValue',
                data: 'someData',
            };
            const converted = convertFromDTO(obj);

            expect(converted).toEqual({modelType: 'someValue', data: 'someData'});
        });

        it('Не мутирует значения по ключу', () => {
            const obj = {
                experiment: 2000,
                someKey: 213,
                converted: 'test - value',
            };
            const converted = convertFromDTO(obj);

            expect(converted.experiment).toBe(2000);
            expect(converted.someKey).toBe(213);
            expect(converted.converted).toBe('test - value');
        });

        it('Преобразовывает и не мутирует значения по ключу', () => {
            const obj = {
                model_type: ['asdasd', 'asdasd'],
            };
            const converted = convertFromDTO(obj);

            expect(converted).toEqual({modelType: ['asdasd', 'asdasd']});
            expect(converted.modelType).toEqual(['asdasd', 'asdasd']);
        });

        it('Тест на все ключи', () => {
            const expectObj = {
                modelType: 'model_type',
                simulation: 'sim_type',
                experimentalData: 'experimental_data',
                processNode: 'process_node',
                voltage: 'supply_voltage',
                diffCoeff: 'diff_coefficient',
                ambipolarDiffCoeff: 'ambipolar_diff_coefficient',
                hasExperiments: 'has_running_experiments',
            };
            const obj = {
                model_type: 'model_type',
                sim_type: 'sim_type',
                experimental_data: 'experimental_data',
                process_node: 'process_node',
                supply_voltage: 'supply_voltage',
                diff_coefficient: 'diff_coefficient',
                ambipolar_diff_coefficient: 'ambipolar_diff_coefficient',
                has_running_experiments: 'has_running_experiments',
            };
            const converted = convertFromDTO(obj);

            expect(converted).toEqual(expectObj);
            expect(Object.keys(obj).length).toBe(Object.keys(expectObj).length);
        });

        it('Тест на Юзера', () => {
            const expObj = {
                hasExperiments: 'has_running_experiments',
                avatar: 'user_avatar',
                lastName: 'last_name',
                firstName: 'first_name',
            };
            const obj = {
                has_running_experiments: 'has_running_experiments',
                user_avatar: 'user_avatar',
                last_name: 'last_name',
                first_name: 'first_name',
            };
            const converted = convertFromDTO(obj);

            expect(converted).toEqual(expObj);
            expect(Object.keys(converted).length).toBe(Object.keys(expObj).length);
        });

        it('Тест на socketDTO', () => {
            const expObj = {
                experimentId: 'exp_id',
            };
            const obj = {
                exp_id: 'exp_id',
            };
            const converted = convertFromDTO(obj);

            expect(converted).toEqual(expObj);
            expect(Object.keys(converted).length).toBe(Object.keys(expObj).length);
        });

        it('Тест на deviceDTO', () => {
            const expObj = {
                processNode: 'process_node',
                voltage: 'supply_voltage',
            };
            const obj = {
                process_node: 'process_node',
                supply_voltage: 'supply_voltage',
            };
            const converted = convertFromDTO(obj);

            expect(converted).toEqual(expObj);
            expect(Object.keys(converted).length).toBe(Object.keys(expObj).length);
        });

        it('Тест на ResultExperimentDTO', () => {
            const expObj = {
                trialsCount: 'trials_count',
                particlesCount: 'particles_count',
                letValuesCount: 'let_values_count',
                simulationResult: 'simulation_result',
                experimentalDataFile: 'data_file_name',
                spectreFile: 'spectre_file_name',
            };
            const obj = {
                trials_count: 'trials_count',
                particles_count: 'particles_count',
                let_values_count: 'let_values_count',
                simulation_result: 'simulation_result',
                data_file_name: 'data_file_name',
                spectre_file_name: 'spectre_file_name',
            };
            const converted = convertFromDTO(obj);

            expect(converted).toEqual(expObj);
            expect(Object.keys(converted).length).toBe(Object.keys(expObj).length);
        });

        it('Тест на experimentDTO', () => {
            const expObj = {
                createdExperiment: 'created_at',
                modelType: 'model_type',
                simulation: 'sim_type',
                experimentalData: 'experimental_data',
                diffCoeff: 'diff_coefficient',
                ambipolarDiffCoeff: 'ambipolar_diff_coefficient',
                calculatingTime: 'calculating_duration',
            };
            const obj = {
                created_at: 'created_at',
                model_type: 'model_type',
                sim_type: 'sim_type',
                experimental_data: 'experimental_data',
                diff_coefficient: 'diff_coefficient',
                ambipolar_diff_coefficient: 'ambipolar_diff_coefficient',
                calculating_duration: 'calculating_duration',
            };
            const converted = convertFromDTO(obj);

            expect(converted).toEqual(expObj);
            expect(Object.keys(converted).length).toBe(Object.keys(expObj).length);
        });
    });
});
