from django.db import models
from django.contrib.auth.models import AbstractUser, Group
from django.db.models.signals import post_save
from django.dispatch import receiver


class User(AbstractUser):
    # bio
    bio = models.TextField(max_length=500, blank=True, null=True)
    user_avatar = models.ImageField(
        upload_to='profile_avatar/',
        default='profile_avatar/default.png',
        blank=True)
    # security
    last_password_update = models.DateTimeField(auto_now_add=True)


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        instance.groups.add(Group.objects.get(name='base_access'))
