import {NgModule} from '@angular/core';

import {RouterModule, Routes} from '@angular/router';
import {ExperimentComponent} from './experiment.component';

const route: Routes = [
    {
        path: '',
        component: ExperimentComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(route)],
    exports: [RouterModule],
})
export class ExperimentRoutingModule {}
