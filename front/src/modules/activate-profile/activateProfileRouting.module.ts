import {NgModule} from '@angular/core';

import {RouterModule, Routes} from '@angular/router';
import {ActivateProfileComponent} from './activate-profile.component';

const activateRoute: Routes = [
    {
        path: ':uid/:token',
        component: ActivateProfileComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(activateRoute)],
    exports: [RouterModule],
})
export class ActivateProfileRoutingModule {}
