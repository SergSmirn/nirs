import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {UserStateService} from '../services/user-state.service';
import {AuthService} from '../services/auth.service';

@Injectable({
    providedIn: 'root',
})
export class NonAuthZoneGuard implements CanActivate {
    constructor(
        private readonly authService: AuthService,
        private readonly userStateService: UserStateService,
    ) {}

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
    ): Observable<boolean> | Promise<boolean> | boolean {
        return this.userStateService.user$.pipe(map(user => !user || !user.isAuth));
    }
}
