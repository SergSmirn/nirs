import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    ViewChild,
} from '@angular/core';
import {
    FormBuilder,
    FormControl,
    FormGroup,
    FormGroupDirective,
    Validators,
} from '@angular/forms';
import {
    EMAIL_MAX_LENGTH,
    emailValidator,
    getErrors,
    matchingPasswords,
    PASSWORD_MAX_LENGTH,
    PASSWORD_MIN_LENGTH,
    passwordValidator,
    USERNAME_MAX_LENGTH,
    USERNAME_MIN_LENGTH,
    usernameValidator,
} from '../../utils/formValidators';
import {AuthService} from '../../services/auth.service';
import {IUser} from '../../models/user.model';
import {MatSnackBar} from '@angular/material';
import {SnackBarPreviewComponent} from '../../shared/modules/snack-bar-preview/snack-bar-preview.component';

const DEFAULT_ERROR_MESSAGE = 'Ошибка регистрации, попробуйте еще раз.';

interface IErrorRegistrationResponse {
    username: string[];
    email: string[];
    password: string[];
}

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SignupComponent {
    form: FormGroup;
    getErrors: Function;
    showLoader = false;

    @ViewChild(FormGroupDirective) private _form;

    constructor(
        private readonly fb: FormBuilder,
        private readonly cd: ChangeDetectorRef,
        private readonly authService: AuthService,
        private readonly snackBar: MatSnackBar,
    ) {
        this.form = this.fb.group(
            {
                username: [
                    null,
                    [
                        Validators.required,
                        Validators.minLength(USERNAME_MIN_LENGTH),
                        Validators.maxLength(USERNAME_MAX_LENGTH),
                        usernameValidator,
                    ],
                ],
                email: [
                    null,
                    [
                        Validators.required,
                        Validators.maxLength(EMAIL_MAX_LENGTH),
                        emailValidator,
                    ],
                ],
                password: [
                    null,
                    [
                        Validators.required,
                        Validators.minLength(PASSWORD_MIN_LENGTH),
                        Validators.maxLength(PASSWORD_MAX_LENGTH),
                        passwordValidator,
                    ],
                ],
                passwordConfirm: [
                    null,
                    [
                        Validators.required,
                        Validators.minLength(PASSWORD_MIN_LENGTH),
                        Validators.maxLength(PASSWORD_MAX_LENGTH),
                        passwordValidator,
                    ],
                ],
            },
            {validator: matchingPasswords('password', 'passwordConfirm')},
        );

        this.getErrors = getErrors;
    }

    get username(): FormControl {
        return <FormControl>this.form.get('username');
    }

    get email(): FormControl {
        return <FormControl>this.form.get('email');
    }

    get password(): FormControl {
        return <FormControl>this.form.get('password');
    }

    get passwordConfirm(): FormControl {
        return <FormControl>this.form.get('passwordConfirm');
    }

    onSubmit() {
        Object.keys(this.form.controls).forEach(key => {
            this.form.get(key).markAsTouched();
        });

        if (this.form.invalid) {
            return;
        }

        this.showLoader = true;
        this.cd.markForCheck();

        const user = <IUser>{
            username: this.username.value,
            password: this.password.value,
            email: this.email.value,
        };

        this.authService.signup(user).subscribe(
            ({email}) => {
                const message = `Для подтверждения регистрации пройдите по ссылке, отправленной на Вашу почту - ${email}`;

                this.showLoader = !this.showLoader;
                this._form.resetForm();

                this.snackBar.openFromComponent(SnackBarPreviewComponent, {
                    verticalPosition: 'top',
                    horizontalPosition: 'right',
                    duration: 7000,
                    data: {message, closer: true, status: 'info'},
                });

                this.cd.markForCheck();
            },
            ({status, error}) => {
                const message = this.getMessage(error);

                this.snackBar.openFromComponent(SnackBarPreviewComponent, {
                    verticalPosition: 'top',
                    horizontalPosition: 'right',
                    duration: 5000,
                    data: {message},
                });

                this.showLoader = !this.showLoader;
                this.cd.markForCheck();
            },
        );
    }

    private getMessage(error?: IErrorRegistrationResponse): string {
        if (error['username'] && error['email']) {
            return 'Имя пользователя и почта заняты';
        }

        if (error['username']) {
            return 'Имя пользователя занято';
        }

        if (error['email']) {
            return 'Почта занята';
        }

        return DEFAULT_ERROR_MESSAGE;
    }
}
