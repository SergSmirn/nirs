import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatDialogModule, MatIconModule} from '@angular/material';
import {SnackBarPreviewComponent} from './snack-bar-preview.component';
import {ResultDialogPreviewModule} from '../../../modules/results/result-dialog-preview/result-dialog-preview.module';
import {ResultDialogPreviewComponent} from '../../../modules/results/result-dialog-preview/result-dialog-preview.component';

@NgModule({
    declarations: [SnackBarPreviewComponent],
    imports: [CommonModule, MatDialogModule, MatIconModule, ResultDialogPreviewModule],
    exports: [SnackBarPreviewComponent],
    entryComponents: [ResultDialogPreviewComponent],
})
export class SnackBarPreviewModule {}
