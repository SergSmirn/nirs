import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TextFilePreviewComponent} from './text-file-preview.component';

@NgModule({
    declarations: [TextFilePreviewComponent],
    imports: [CommonModule],
    exports: [TextFilePreviewComponent],
})
export class TextFilePreviewModule {}
