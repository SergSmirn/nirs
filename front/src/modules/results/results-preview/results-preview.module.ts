import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatDialogModule} from '@angular/material';
import {ResultsPreviewComponent} from './results-preview.component';
import {SpectreDialogPreviewComponent} from '../spectre-dialog-preview/spectre-dialog-preview.component';
import {SpectreDialogPreviewModule} from '../spectre-dialog-preview/spectre-dialog-preview.module';
import {GraphDialogPreviewComponent} from '../graph-dialog-preview/graph-dialog-preview.component';
import {GraphDialogPreviewModule} from '../graph-dialog-preview/graph-dialog-preview.module';
import {ButtonModule} from '../../../shared/modules/button/button.module';
import {LoaderModule} from '../../../shared/modules/loader/loader.module';
import {ConfirmDialogModule} from '../../../shared/modules/confirm-dialog/confirm-dialog.module';
import {ResultTranslatePipe} from '../../../pipes/result-translate.pipe';
import {ConfirmDialogComponent} from '../../../shared/modules/confirm-dialog/confirm-dialog.component';
import {DimensionModule} from '../../../shared/modules/dimension/dimension.module';
import {TextFilePreviewComponent} from '../../../shared/modules/text-file-preview/text-file-preview.component';
import {TextFilePreviewModule} from '../../../shared/modules/text-file-preview/text-file-preview.module';

@NgModule({
    declarations: [ResultsPreviewComponent, ResultTranslatePipe],
    imports: [
        CommonModule,
        MatDialogModule,
        ButtonModule,
        LoaderModule,
        SpectreDialogPreviewModule,
        GraphDialogPreviewModule,
        ConfirmDialogModule,
        DimensionModule,
        TextFilePreviewModule,
    ],
    exports: [ResultsPreviewComponent],
    entryComponents: [
        SpectreDialogPreviewComponent,
        GraphDialogPreviewComponent,
        ConfirmDialogComponent,
        TextFilePreviewComponent,
    ],
})
export class ResultsPreviewModule {}
