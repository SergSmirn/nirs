import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SignupComponent} from './signup.component';
import {SignupRoutingModule} from './signupRouting.module';
import {ButtonModule} from '../../shared/modules/button/button.module';
import {ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule, MatInputModule, MatSnackBarModule} from '@angular/material';
import {LoaderModule} from '../../shared/modules/loader/loader.module';
import {AutoFocusModule} from '../../directives/auto-focus.module';
import {SnackBarPreviewComponent} from '../../shared/modules/snack-bar-preview/snack-bar-preview.component';
import {SnackBarPreviewModule} from '../../shared/modules/snack-bar-preview/snack-bar-preview.module';

@NgModule({
    declarations: [SignupComponent],
    imports: [
        CommonModule,
        SignupRoutingModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSnackBarModule,
        ButtonModule,
        LoaderModule,
        AutoFocusModule,
        SnackBarPreviewModule,
    ],
    entryComponents: [SnackBarPreviewComponent],
})
export class SignupModule {}
