import {AbstractBaseExperiment, IControls} from './abstract-base-experiment';
import {GroupType, User} from '../../models/user.model';
import {FormControl, Validators} from '@angular/forms';

const experimentControls: IControls = {
    modelType: {
        value: 'diffusion',
        validators: [Validators.required],
    },
    simulation: {
        value: 'analytical',
        validators: [Validators.required],
    },
    geometry: {
        value: 'disc',
        validators: [Validators.required],
    },
    diffCoeff: {
        value: 12,
        validators: [Validators.min(1), Validators.max(30), Validators.required],
    },
    ambipolarDiffCoeff: {
        value: 25,
        validators: [Validators.min(1), Validators.max(30), Validators.required],
    },
};

export const deviceControls: IControls = {
    processNode: {
        value: null,
        validators: [Validators.required, Validators.min(7), Validators.max(1000)],
    },
    voltage: {
        value: 8,
        validators: [Validators.required, Validators.min(0.1), Validators.max(20)],
    },
    resistance: {
        value: 15000,
        validators: [Validators.required, Validators.min(1000), Validators.max(20000)],
    },
    capacitance: {
        value: 1e-15,
        validators: [Validators.required, Validators.min(0), Validators.max(1)],
    },
};

interface IFullExperimentControls {
    readonly modelType: FormControl | null;
    readonly simulation: FormControl | null;
    readonly geometry: FormControl | null;
    readonly diffCoeff: FormControl | null;
    readonly ambipolarDiffCoeff: FormControl | null;
}

export class AbstractExperiment extends AbstractBaseExperiment
    implements IFullExperimentControls {
    private readonly isBaseUser: boolean;

    constructor(protected readonly user: User) {
        super();

        this.isBaseUser = user.group === GroupType.base;

        if (!this.isBaseUser) {
            this.setControls(experimentControls);
            // this.setControls(deviceControls);
        }
    }

    get modelType(): FormControl | null {
        return <FormControl>this.form.get('modelType') || null;
    }

    get simulation(): FormControl | null {
        return <FormControl>this.form.get('simulation') || null;
    }

    get geometry(): FormControl | null {
        return <FormControl>this.form.get('geometry') || null;
    }

    get diffCoeff(): FormControl | null {
        return <FormControl>this.form.get('diffCoeff') || null;
    }

    get ambipolarDiffCoeff(): FormControl | null {
        return <FormControl>this.form.get('ambipolarDiffCoeff') || null;
    }

    // device
    get processNode(): FormControl | null {
        return <FormControl>this.newDevice.get('processNode') || null;
    }

    get voltage(): FormControl | null {
        return <FormControl>this.newDevice.get('voltage') || null;
    }

    get resistance(): FormControl | null {
        return <FormControl>this.newDevice.get('resistance') || null;
    }

    get capacitance(): FormControl | null {
        return <FormControl>this.newDevice.get('capacitance') || null;
    }

    protected markAsTouched() {
        super.markAsTouched();

        if (this.user.group === GroupType.base) {
            return;
        }

        Object.keys(experimentControls).forEach(controlKey => {
            const control = this.form.get(controlKey);

            if (control) {
                control.markAsTouched();
            }
        });

        if (!this.newDevice) {
            return;
        }

        Object.keys(deviceControls).forEach(controlKey => {
            const control = this.newDevice.get(controlKey);

            if (control) {
                control.markAsTouched();
            }
        });
    }

    protected getToDtoExperiment(formData: FormData): FormData {
        const form = super.getToDtoExperiment(formData);

        if (!this.isBaseUser) {
            formData.append('model_type', this.modelType.value);
            formData.append('sim_type', this.simulation.value);
            formData.append('geometry', this.geometry.value);
            formData.append('diff_coefficient', this.diffCoeff.value);
            formData.append('ambipolar_diff_coefficient', this.ambipolarDiffCoeff.value);
        }

        return form;
    }
}
