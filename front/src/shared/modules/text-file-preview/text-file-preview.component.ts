import {ChangeDetectionStrategy, Component, Inject, Optional} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';

@Component({
    selector: 'app-text-file-preview',
    templateUrl: './text-file-preview.component.html',
    styleUrls: ['./text-file-preview.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TextFilePreviewComponent {
    constructor(
        @Optional() @Inject(MAT_DIALOG_DATA) public readonly data: string | null,
    ) {}
}
