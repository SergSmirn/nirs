const experimentDTO = {
    createdExperiment: 'created_at',
    modelType: 'model_type',
    simulation: 'sim_type',
    experimentalData: 'experimental_data',
    diffCoeff: 'diff_coefficient',
    ambipolarDiffCoeff: 'ambipolar_diff_coefficient',
    calculatingTime: 'calculating_duration',
};

const ResultExperimentDTO = {
    trialsCount: 'trials_count',
    particlesCount: 'particles_count',
    letValuesCount: 'let_values_count',
    simulationResult: 'cross_section',
    experimentalDataFile: 'data_file_name',
    spectreFile: 'spectre_file_name',
    spectreFileText: 'spectre_file_text',
};

const deviceDTO = {
    processNode: 'process_node',
    voltage: 'supply_voltage',
};

const socketDTO = {
    id: 'exp_id',
};

const userDTO = {
    hasExperiments: 'has_running_experiments',
    lastName: 'last_name',
    firstName: 'first_name',
    avatar: 'user_avatar',
    isAuthenticated: 'is_authenticated',
    userData: 'user_data',
};

const convertObj = {
    ...experimentDTO,
    ...deviceDTO,
    ...socketDTO,
    ...userDTO,
    ...ResultExperimentDTO,
};

export function convertToDTO(obj: any) {
    const isArray = Array.isArray(obj);
    const newObj = isArray ? [] : {};

    Object.keys(obj).forEach(key => {
        const dtoKey = convertObj[key] ? convertObj[key] : key;
        const value =
            typeof obj[key] === 'object' && obj[key] !== null
                ? convertToDTO(obj[key])
                : obj[key];

        if (isArray) {
            (newObj as Array<any>).push(value);
        } else {
            newObj[dtoKey] = value;
        }
    });

    return newObj;
}

export function convertFromDTO<T>(obj: T): T {
    const isArray = Array.isArray(obj);
    const newObj = isArray ? [] : {};

    Object.keys(obj).forEach(key => {
        const frontKey =
            Object.keys(convertObj).find(convertKey => convertObj[convertKey] === key) ||
            key;
        const value = obj[key];
        const frontValue =
            typeof value === 'object' && value !== null ? convertFromDTO(value) : value;

        if (isArray) {
            (newObj as Array<any>).push(frontValue);
        } else {
            newObj[frontKey] = frontValue;
        }
    });

    return newObj as T;
}
