import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {IExperiment} from '../../../models/experiments.model';
import {ExperimentsService} from '../../../services/experiments.service';

@Component({
    selector: 'app-result-dialog-preview',
    templateUrl: './result-dialog-preview.component.html',
    styleUrls: ['./result-dialog-preview.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResultDialogPreviewComponent {
    experiment$: Observable<IExperiment>;

    constructor(
        private dialogRef: MatDialogRef<ResultDialogPreviewComponent>,
        @Inject(MAT_DIALOG_DATA) private readonly data: any,
        private readonly experimentsService: ExperimentsService,
    ) {
        const {id} = data;

        this.experimentsService.getExperimentResult(id);
        this.experiment$ = this.experimentsService.experiments$.pipe(
            map(experiments => experiments.find(experiment => experiment.id === id)),
        );
    }

    close() {
        this.dialogRef.close();
    }
}
