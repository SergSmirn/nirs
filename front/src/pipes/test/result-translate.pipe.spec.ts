import {ResultTranslatePipe} from '../result-translate.pipe';

describe('ResultTranslatePipe', () => {
    let pipe;

    beforeEach(() => {
        pipe = new ResultTranslatePipe();
    });

    it('create an instance', () => {
        expect(pipe).toBeTruthy();
    });

    it('Если нет в вариантах для перевода - остается неизменным', () => {
        const value = 'different';

        expect(pipe.transform(value)).toBe('different');
    });

    it('Переводит point', () => {
        const value = 'point';

        expect(pipe.transform(value)).toBe('Точечная');
    });

    it('Переводит diffusion', () => {
        const value = 'diffusion';

        expect(pipe.transform(value)).toBe('Диффузионная');
    });
    it('Переводит analytical', () => {
        const value = 'analytical';

        expect(pipe.transform(value)).toBe('Аналитический');
    });

    it('Переводит monte_carlo', () => {
        const value = 'monte_carlo';

        expect(pipe.transform(value)).toBe('Монте-Карло');
    });
    it('Переводит disc', () => {
        const value = 'disc';

        expect(pipe.transform(value)).toBe('Диск');
    });

    it('Переводит sphere', () => {
        const value = 'sphere';

        expect(pipe.transform(value)).toBe('Сфера');
    });
});
