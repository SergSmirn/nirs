import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {map, tap} from 'rxjs/operators';
import {IUser, IUserFromDto, User} from '../models/user.model';
import {UserStateService} from './user-state.service';
import {convertFromDTO} from '../utils/convertDTO';
import {ExperimentsService} from './experiments.service';

@Injectable({
    providedIn: 'root',
})
export class AuthService {
    redirectUrl: string;

    constructor(
        private readonly http: HttpClient,
        private readonly experimentsService: ExperimentsService,
        private readonly userStateService: UserStateService,
        private readonly router: Router,
    ) {}

    init(): Observable<User> {
        return this.http.get<IUserFromDto>('/api/init/', {withCredentials: true}).pipe(
            map(convertFromDTO),
            map(({userData, isAuthenticated}) => new User(userData, isAuthenticated)),
            tap(user => (this.userStateService.user = user)),
            tap(user => {
                if (user.isAuth && user.info.hasExperiments) {
                    this.experimentsService.centrifugo();
                }
            }),
        );
    }

    signup(user: IUser): Observable<IUser> {
        return this.http
            .post<IUser>('/api/signup/', user, {withCredentials: true})
            .pipe(map(convertFromDTO));
    }

    activate({uid, token}): Observable<any> {
        const url = `/api/activate/${uid}/${token}/`;

        return this.http.get(url, {withCredentials: true});
    }

    login(user: IUser): Observable<User> {
        return this.http.post<IUser>('/api/login/', user, {withCredentials: true}).pipe(
            map(convertFromDTO),
            map((loggedUser: IUser) => new User(loggedUser, true)),
            tap(loggedUser => (this.userStateService.user = loggedUser)),
            tap(() => {
                const route = this.redirectUrl || '';

                this.router.navigate([route]);
            }),
        );
    }

    logout(): Observable<any> {
        return this.http.post('/api/logout/', null, {withCredentials: true}).pipe(
            tap(() => (this.userStateService.user = null)),
            tap(() => this.router.navigate([''])),
        );
    }
}
