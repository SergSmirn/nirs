import {Injectable} from '@angular/core';
import {
    ActivatedRouteSnapshot,
    CanActivate,
    Router,
    RouterStateSnapshot,
} from '@angular/router';
import {Observable, of} from 'rxjs';
import {map, switchMap, tap} from 'rxjs/operators';
import {UserStateService} from '../services/user-state.service';
import {AuthService} from '../services/auth.service';

@Injectable({
    providedIn: 'root',
})
export class AuthGuard implements CanActivate {
    constructor(
        private readonly authService: AuthService,
        private readonly userStateService: UserStateService,
        private readonly router: Router,
    ) {}

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
    ): Observable<boolean> | Promise<boolean> | boolean {
        return this.userStateService.user$.pipe(
            switchMap(user => (!user ? this.init : of(user.isAuth))),
            tap(isAuth => {
                if (!isAuth) {
                    this.router.navigate(['/login']);
                    this.authService.redirectUrl = state.url;
                }
            }),
        );
    }

    private get init(): Observable<boolean> {
        return this.authService.init().pipe(map(initUser => initUser.isAuth));
    }
}
