import {Pipe, PipeTransform} from '@angular/core';
import {IDevice} from '../models/experiments.model';

@Pipe({
    name: 'devicePreview',
})
export class DevicePreviewPipe implements PipeTransform {
    transform(device: IDevice, isBasic: boolean): any {
        return isBasic
            ? device.name
            : `${device.name},
            Норма: ${device.processNode},
            V: ${device.voltage} В,
            R: ${device.resistance} Ом,
            C: ${device.capacitance} Ф`;
    }
}
