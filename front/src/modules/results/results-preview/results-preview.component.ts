import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    Output,
} from '@angular/core';
import {MatDialog} from '@angular/material';
import {Router} from '@angular/router';
import {filter, switchMap, take} from 'rxjs/operators';
import {SpectreDialogPreviewComponent} from '../spectre-dialog-preview/spectre-dialog-preview.component';
import {GraphDialogPreviewComponent} from '../graph-dialog-preview/graph-dialog-preview.component';
import {IResultExperiment} from '../../../models/experiments.model';
import {UserStateService} from '../../../services/user-state.service';
import {ExperimentsService} from '../../../services/experiments.service';
import {ConfirmDialogComponent} from '../../../shared/modules/confirm-dialog/confirm-dialog.component';
import {GroupType} from '../../../models/user.model';
import {TextFilePreviewComponent} from '../../../shared/modules/text-file-preview/text-file-preview.component';

@Component({
    selector: 'app-results-preview',
    templateUrl: './results-preview.component.html',
    styleUrls: ['./results-preview.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResultsPreviewComponent {
    @Input() experiment: IResultExperiment;

    @Output() onRunAgain = new EventEmitter<void>();

    isBasic: boolean;

    constructor(
        private readonly dialog: MatDialog,
        private readonly router: Router,
        private readonly userStateService: UserStateService,
        private readonly experimentsService: ExperimentsService,
    ) {
        this.isBasic = userStateService.user.group === GroupType.base;
    }

    get disabled(): boolean {
        return !this.experiment || !this.experiment.calculatingTime;
    }

    showGraph() {
        if (this.disabled) {
            return;
        }

        this.dialog.open(GraphDialogPreviewComponent, {
            width: '80%',
            data: {
                simulationResult: this.experiment.simulationResult,
                experimentalData: this.experiment.experimentalData,
                experimentalDataFile: this.experiment.experimentalDataFile,
            },
        });
    }

    showSpectre() {
        if (this.disabled) {
            return;
        }

        this.dialog.open(SpectreDialogPreviewComponent, {
            data: this.experiment.simulationResult,
            width: '80%',
            height: '90%',
        });
    }

    runAgain() {
        this.router.navigate(['experiment'], {queryParams: {id: this.experiment.id}});

        this.onRunAgain.emit();
    }

    delete() {
        if (this.disabled) {
            return;
        }

        this.dialog
            .open(ConfirmDialogComponent, {
                width: '500px',
                disableClose: true,
            })
            .afterClosed()
            .pipe(
                take(1),
                filter((response: boolean) => response),
                switchMap(() =>
                    this.experimentsService.deleteExperiment(this.experiment.id),
                ),
            )
            .subscribe();
    }

    openTextFilePreview(fileKey: keyof IResultExperiment) {
        const data = this.experiment[fileKey];

        this.dialog.open(TextFilePreviewComponent, {
            width: '700px',
            height: '90%',
            data,
        });
    }
}
