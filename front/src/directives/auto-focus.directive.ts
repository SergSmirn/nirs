import {AfterViewInit, Directive, ElementRef} from '@angular/core';

@Directive({
    selector: '[appAutoFocus]',
})
export class AutoFocusDirective implements AfterViewInit {
    constructor(private readonly elementRef: ElementRef) {}

    ngAfterViewInit() {
        setTimeout(() => this.elementRef.nativeElement.focus(), 1);
    }
}
