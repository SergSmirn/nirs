import {Routes} from '@angular/router';
import {NotFoundComponent} from '../not-found/not-found.component';
import {NonAuthZoneGuard} from '../../guard/nonAuthZone.guard';
import {AuthGuard} from '../../guard/auth.guard';

export const appRoutes: Routes = [
    {
        path: 'login',
        canActivate: [NonAuthZoneGuard],
        loadChildren: '../login/login.module#LoginModule',
    },
    {
        path: 'signup',
        canActivate: [NonAuthZoneGuard],
        loadChildren: '../signup/signup.module#SignupModule',
    },
    {
        path: 'activate',
        canActivate: [NonAuthZoneGuard],
        loadChildren: '../activate-profile/activate-profile.module#ActivateProfileModule',
    },
    {
        path: 'experiment',
        canActivate: [AuthGuard],
        loadChildren: '../experiment/experiment.module#ExperimentModule',
    },
    {
        path: 'results',
        canActivate: [AuthGuard],
        loadChildren: '../results/results.module#ResultsModule',
    },
    {
        path: '',
        pathMatch: 'full',
        loadChildren: '../home/home.module#HomeModule',
    },
    {
        path: '**',
        component: NotFoundComponent,
    },
];
