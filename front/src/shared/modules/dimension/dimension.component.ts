import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

export enum DimensionSigns {
    plus,
    minus,
    multiply,
    division,
}

export interface IDimension {
    dimension: string;
    power?: number;
    index?: number;
    sign?: DimensionSigns;
}

@Component({
    selector: 'app-dimension',
    templateUrl: './dimension.component.html',
    styleUrls: ['./dimension.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DimensionComponent {
    @Input() items: IDimension[];

    getSign(sign: DimensionSigns): string {
        switch (sign) {
            case DimensionSigns.plus:
                return '+';
            case DimensionSigns.minus:
                return '-';
            case DimensionSigns.multiply:
                return '*';
            case DimensionSigns.division:
                return '/';
            default:
                return '';
        }
    }
}
