#!/bin/sh
python manage.py migrate --no-input
python manage.py prepare_db
python /code/src/manage.py runserver 0.0.0.0:8000
exec "$@"