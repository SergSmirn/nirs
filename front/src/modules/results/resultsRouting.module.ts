import {NgModule} from '@angular/core';

import {RouterModule, Routes} from '@angular/router';
import {ResultsComponent} from './results.component';

const resultsRoute: Routes = [
    {
        path: '',
        component: ResultsComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(resultsRoute)],
    exports: [RouterModule],
})
export class ResultsRoutingModule {}
