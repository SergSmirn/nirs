import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {HttpClientModule, HttpClientXsrfModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatSnackBarModule} from '@angular/material';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './appRouting.module';
import {NavigationModule} from '../../shared/modules/navigation/navigation.module';
import {NotFoundModule} from '../not-found/not-found.module';
import {SnackBarPreviewComponent} from '../../shared/modules/snack-bar-preview/snack-bar-preview.component';
import {SnackBarPreviewModule} from '../../shared/modules/snack-bar-preview/snack-bar-preview.module';

@NgModule({
    declarations: [AppComponent],
    imports: [
        AppRoutingModule,
        BrowserModule,
        BrowserAnimationsModule,
        RouterModule,
        HttpClientModule,
        HttpClientXsrfModule.withOptions({
            cookieName: 'csrftoken',
            headerName: 'X-CSRFToken',
        }),
        NavigationModule,
        NotFoundModule,
        MatSnackBarModule,
        SnackBarPreviewModule,
    ],
    bootstrap: [AppComponent],
    entryComponents: [SnackBarPreviewComponent],
})
export class AppModule {}
