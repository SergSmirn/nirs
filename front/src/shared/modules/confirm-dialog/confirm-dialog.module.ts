import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ConfirmDialogComponent} from './confirm-dialog.component';
import {ButtonModule} from '../button/button.module';
import {MatIconModule} from '@angular/material';

@NgModule({
    declarations: [ConfirmDialogComponent],
    imports: [CommonModule, MatIconModule, ButtonModule],
})
export class ConfirmDialogModule {}
