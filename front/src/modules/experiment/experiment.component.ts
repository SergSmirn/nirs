import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    OnDestroy,
    OnInit,
    ViewChild,
} from '@angular/core';
import {
    FormArray,
    FormBuilder,
    FormControl,
    FormGroup,
    FormGroupDirective,
    Validators,
} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {MatDialog, MatSlideToggleChange, MatSnackBar} from '@angular/material';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {filter, map, switchMap, tap} from 'rxjs/operators';
import {getErrors} from '../../utils/formValidators';
import {ExperimentsService} from '../../services/experiments.service';
import {IDevice, IResultExperiment} from '../../models/experiments.model';
import {UserStateService} from '../../services/user-state.service';
import {convertFromDTO} from '../../utils/convertDTO';
import {AbstractExperiment, deviceControls} from './abstract-experiment';
import {takeUntil} from 'rxjs/internal/operators';
import {
    deviceBaseControl,
    deviceBaseControls,
    experimentBaseControls,
} from './abstract-base-experiment';
import {GroupType} from '../../models/user.model';

const MIN_MANUAL_INPUTS = 2;

@Component({
    selector: 'app-experiment',
    templateUrl: './experiment.component.html',
    styleUrls: ['./experiment.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ExperimentComponent extends AbstractExperiment implements OnInit, OnDestroy {
    readonly showLoader$ = new BehaviorSubject<boolean>(false);
    getErrors = getErrors;
    devices$: Observable<IDevice[]>;
    isBasicUser: boolean;
    isManualInput = false;
    readonly minManualInputs = MIN_MANUAL_INPUTS;
    readonly allowTypes = [
        'text/plain',
        'text/csv',
        'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    ];

    @ViewChild(FormGroupDirective)
    private readonly nativeForm: FormGroupDirective;

    private id: number = null;

    private fileBuffer: any = null;

    private manualInputBuffer: {lpe: number; section: number}[] = [
        {lpe: null, section: null},
        {lpe: null, section: null},
    ];

    private readonly destroy$ = new Subject<void>();

    constructor(
        private readonly dialog: MatDialog,
        private readonly cd: ChangeDetectorRef,
        private readonly formBuilder: FormBuilder,
        private readonly activatedRoute: ActivatedRoute,
        private readonly snackBar: MatSnackBar,
        private readonly userStateService: UserStateService,
        private readonly experimentsService: ExperimentsService,
    ) {
        super(userStateService.user);

        this.isBasicUser = userStateService.user.group === GroupType.base;
    }

    get experimentalDataManual(): FormArray | null {
        return <FormArray>this.form.get('experimentalDataManual') || null;
    }

    ngOnInit() {
        const query = this.activatedRoute.snapshot.queryParams;
        const id = query.id && parseInt(query.id, 10);

        if (!isNaN(id)) {
            this.experimentsService.getExperimentResult(id);

            this.experimentsService.experiments$
                .pipe(
                    filter(experiments => !!experiments),
                    map(experiments => {
                        return experiments.find(experiment => experiment.id === id);
                    }),
                    takeUntil(this.destroy$),
                )
                .subscribe(this.setValueInForm.bind(this));
        }

        this.experimentsService.updateDevicesStorage();
        this.devices$ = this.experimentsService.devices$;
    }

    ngOnDestroy() {
        this.destroy$.next();
    }

    getControlInFormArray(index: number, name: string): FormControl {
        return <FormControl>(
            (<FormGroup>this.experimentalDataManual.controls[index]).controls[name]
        );
    }

    addNewManualControl() {
        const newManualControl = this.createExperimentalDataControls();

        this.experimentalDataManual.push(newManualControl);
    }

    removeManualControls(index: number) {
        if (this.experimentalDataManual.controls.length === MIN_MANUAL_INPUTS) {
            return;
        }

        this.experimentalDataManual.removeAt(index);
    }

    onNewDevice() {
        if (!this.device) {
            this.form.removeControl('newDevice');
            this.setControls(deviceBaseControl);

            return;
        }

        this.form.removeControl('device');

        const deviceConfig = !this.isBasicUser
            ? {...deviceControls, ...deviceBaseControls}
            : deviceBaseControls;
        const controls = this.getControlsByConfig(deviceConfig);
        const form = this.formBuilder.group(controls);

        this.form.addControl('newDevice', form);
    }

    clearForm() {
        this.nativeForm.resetForm();
    }

    changeExperimentalDataType({checked: isManualInput}: MatSlideToggleChange) {
        this.isManualInput = isManualInput;

        if (isManualInput) {
            this.fileBuffer =
                (this.experimentalData && this.experimentalData.value) || null;
            this.form.removeControl('experimentalData');

            this.pushExperimentalDataControls();
        } else {
            const controlConfig = experimentBaseControls.experimentalData;
            const experimentalDataControl = new FormControl();

            experimentalDataControl.setValue(controlConfig.value);
            experimentalDataControl.setValidators(controlConfig.validators);

            this.manualInputBuffer =
                (this.experimentalDataManual && this.experimentalDataManual.value) ||
                this.manualInputBuffer;
            experimentalDataControl.setValue(this.fileBuffer || null);

            this.form.removeControl('experimentalDataManual');
            this.form.addControl('experimentalData', experimentalDataControl);
        }
    }

    onSubmit(event: any) {
        event.preventDefault();

        this.markAsTouched();

        if (this.form.invalid) {
            return;
        }

        this.showLoader$.next(true);

        const formData = this.getToDtoExperiment();

        this.experimentsService.centrifugo();

        if (this.newDevice) {
            const device = <IDevice>{...this.newDevice.value};

            this.experimentsService
                .newDevice(device)
                .pipe(
                    switchMap(({id}) => {
                        formData.append('device', `${id}`);

                        return this.getNewExperiment(formData);
                    }),
                    takeUntil(this.destroy$),
                )
                .subscribe(
                    ({status}) => {
                        if (status === 'finished') {
                            this.showLoader$.next(false);
                        }
                    },
                    ({error}) => {
                        this.errorHandler(error);
                        this.showLoader$.next(false);
                    },
                );

            return;
        }

        this.getNewExperiment(formData)
            .pipe(takeUntil(this.destroy$))
            .subscribe(
                ({status}) => {
                    if (status === 'finished') {
                        this.showLoader$.next(false);
                    }
                },
                ({error}) => {
                    this.errorHandler(error);
                    this.showLoader$.next(false);
                },
            );
    }

    protected markAsTouched() {
        super.markAsTouched();

        if (this.isManualInput) {
            Object.keys(this.experimentalDataManual.controls).forEach(index => {
                const lpeControl = this.getControlInFormArray(index as any, 'lpe');
                const sectionControl = this.getControlInFormArray(
                    index as any,
                    'section',
                );

                lpeControl.markAsTouched();
                sectionControl.markAsTouched();
            });
        }

        if (this.newDevice) {
            Object.keys(this.newDevice.controls).forEach(controlKey => {
                const control = this.form.get(controlKey);

                if (control) {
                    control.markAsTouched();
                }
            });
        }
    }

    protected getToDtoExperiment(): FormData {
        const form = super.getToDtoExperiment(new FormData());

        if (this.id) {
            form.append('id', this.id.toString());
        }

        const experimentalData = this.isManualInput
            ? JSON.stringify(
                  this.experimentalDataManual.value.map(({lpe, section}) => [
                      lpe,
                      section,
                  ]),
              )
            : this.experimentalData.value;
        const experimentalDataName = this.isManualInput
            ? 'experimental_data'
            : 'experimental_data_file';

        form.append(experimentalDataName, experimentalData);

        return form;
    }

    private getNewExperiment(formData: FormData): any {
        let newExpId;

        return this.experimentsService.newExperiment(formData).pipe(
            tap(({id}) => {
                newExpId = id;
            }),
            switchMap(() => this.experimentsService.experiments$),
            filter(experiments => !!experiments),
            map(experiments => experiments.find(({id}) => id === newExpId)),
            filter(experiment => !!experiment),
        );
    }

    private setValueInForm(result: IResultExperiment) {
        if (!result) {
            return;
        }

        const {
            id,
            device,
            modelType,
            simulation,
            geometry,
            diffCoeff,
            ambipolarDiffCoeff,
            experimentalData: manualInput,
            experimentalDataFile,
            spectreFile,
        } = result;
        const fullFields = {
            modelType,
            simulation,
            geometry,
            diffCoeff,
            ambipolarDiffCoeff,
        };
        const experimentalData = experimentalDataFile
            ? new File(['xyz'], experimentalDataFile, {
                  type:
                      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
              })
            : manualInput.map(([lpe, section]) => {
                  return {lpe, section};
              });
        const spectre = new File(['xyz'], spectreFile, {type: 'text/plain'});
        this.id = id;

        this.isManualInput = !experimentalDataFile;
        this.changeExperimentalDataType({checked: !experimentalDataFile} as any);
        this.cd.markForCheck();

        const baseFields = {
            device: device.id,
            [`${
                experimentalDataFile ? 'experimentalData' : 'experimentalDataManual'
            }`]: experimentalData,
            spectre,
        };
        const fields = this.isBasicUser ? baseFields : {...baseFields, ...fullFields};

        this.form.patchValue({...fields});
    }

    private errorHandler(error: any) {
        const errorFromDto = convertFromDTO(error);

        if (!errorFromDto) {
            return;
        }

        if (errorFromDto.spectre) {
            this.spectre.setErrors({file: 'badFile'});
        }

        if (errorFromDto.experimentalData && this.experimentalData) {
            this.experimentalData.setErrors({file: 'badFile'});
        }
    }

    private pushExperimentalDataControls() {
        const formArray = this.createExperimentalFormArray();

        this.manualInputBuffer.forEach(({lpe, section}) => {
            formArray.push(this.createExperimentalDataControls(lpe, section));
        });

        this.form.addControl('experimentalDataManual', formArray);
    }

    private createExperimentalFormArray(): FormArray {
        return this.formBuilder.array([]);
    }

    private createExperimentalDataControls(lpe?: number, section?: number): FormGroup {
        return this.formBuilder.group({
            lpe: [lpe || null, [Validators.min(0)]],
            section: [section || null, [Validators.min(0)]],
        });
    }
}
