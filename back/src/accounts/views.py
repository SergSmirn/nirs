from django.contrib.auth import login, authenticate, logout
from django.views.decorators.csrf import ensure_csrf_cookie
from accounts.serializers import UserCreateSerializer, UserShortSerializer
from accounts.models import User
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.core.mail import send_mail
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from accounts.tokens import account_activation_token
from django.conf import settings


@api_view(['POST'])
def signup(request):
    if request.method == 'POST':
        serializer = UserCreateSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save(is_active=False)
            domain = get_current_site(request).domain
            protocol = 'https' if request.is_secure() else 'http'

            message = render_to_string('active_email.html', {
                'domain': domain,
                'protocol': protocol,
                'user': user,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                'token': account_activation_token.make_token(user),
            })
            subject = 'Активация аккаунта'
            to_email = user.email
            send_mail(subject, message, settings.DEFAULT_FORM_EMAIL, [to_email], fail_silently=False)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def activate(request, uidb64, token):
    try:
        user_pk = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=user_pk)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        return Response(status=status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def signin(request):
    username = request.data.get('username')
    password = request.data.get('password')
    user = authenticate(request, username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            serializer = UserShortSerializer(user)
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        return Response(status=status.HTTP_406_NOT_ACCEPTABLE)
    return Response(status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def signout(request):
    logout(request)
    return Response(status=status.HTTP_200_OK)


@api_view(['GET'])
@ensure_csrf_cookie
def init(request):
    user = request.user
    if user.is_authenticated:
        user_serializer = UserShortSerializer(user)
        data = user_serializer.data
    else:
        data = None
    return Response(data={'is_authenticated': user.is_authenticated, 'user_data': data}, status=status.HTTP_200_OK)
