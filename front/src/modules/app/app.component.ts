import {ChangeDetectionStrategy, Component} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {StatusHandlerService} from '../../services/status-handler.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
    constructor(
        private readonly authService: AuthService,
        private readonly statusHandlerService: StatusHandlerService,
    ) {
        this.authService.init().subscribe();
        this.statusHandlerService.initialize();
    }
}
