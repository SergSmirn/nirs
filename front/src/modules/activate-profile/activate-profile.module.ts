import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ActivateProfileComponent} from './activate-profile.component';
import {ActivateProfileRoutingModule} from './activateProfileRouting.module';

@NgModule({
    declarations: [ActivateProfileComponent],
    imports: [CommonModule, ActivateProfileRoutingModule],
})
export class ActivateProfileModule {}
