import {ExperimentStatePipe} from '../experiment-state.pipe';

describe('ExperimentStatePipe', () => {
    let pipe;

    beforeEach(() => {
        pipe = new ExperimentStatePipe();
    });

    it('create an instance', () => {
        expect(pipe).toBeTruthy();
    });

    it('Если прилетит любой отличный от emun статусов - В ожидании', () => {
        const status = 'someState';
        const experiment = <any>{
            status,
        };
        const state = pipe.transform(experiment);

        expect(state).toBe('В ожидании');
    });

    it('Статус - in_queue', () => {
        const status = 'in_queue';
        const experiment = <any>{
            status,
        };
        const state = pipe.transform(experiment);

        expect(state).toBe('В очереди');
    });

    it('Статус - started', () => {
        const status = 'started';
        const experiment = <any>{
            status,
        };
        const state = pipe.transform(experiment);

        expect(state).toBe('Выполняется');
    });

    it('Статус - calculating', () => {
        const status = 'calculating';
        const progress = 5;
        const experiment = <any>{
            status,
            progress,
        };
        const state = pipe.transform(experiment);

        expect(state).toBe('Выполняется, 5%');
    });

    it('Статус - finished', () => {
        const status = 'finished';
        const experiment = <any>{
            status,
        };
        const state = pipe.transform(experiment);

        expect(state).toBe('Завершен');
    });
});
