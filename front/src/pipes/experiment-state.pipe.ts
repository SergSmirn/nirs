import {Pipe, PipeTransform} from '@angular/core';
import {ExperimentStatus, IResultExperiment} from '../models/experiments.model';

@Pipe({
    name: 'experimentState',
})
export class ExperimentStatePipe implements PipeTransform {
    transform(experiment: IResultExperiment): any {
        const {status, progress} = experiment;

        switch (status) {
            case ExperimentStatus.started:
            case ExperimentStatus.calculating:
                return `Выполняется${progress ? ', ' + progress + '%' : ''}`;
            case ExperimentStatus.finished:
                return 'Завершен';
            case ExperimentStatus.inQueue:
                return 'В очереди';
            default:
                return 'В ожидании';
        }
    }
}
