import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoaderComponent} from './loader.component';
import {MatIconModule, MatProgressSpinnerModule} from '@angular/material';

@NgModule({
    declarations: [LoaderComponent],
    imports: [CommonModule, MatProgressSpinnerModule, MatIconModule],
    exports: [LoaderComponent],
})
export class LoaderModule {}
