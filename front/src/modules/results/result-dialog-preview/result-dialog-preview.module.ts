import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ResultDialogPreviewComponent} from './result-dialog-preview.component';
import {ResultsPreviewModule} from '../results-preview/results-preview.module';
import {MatIconModule} from '@angular/material';

@NgModule({
    declarations: [ResultDialogPreviewComponent],
    imports: [CommonModule, ResultsPreviewModule, MatIconModule],
})
export class ResultDialogPreviewModule {}
