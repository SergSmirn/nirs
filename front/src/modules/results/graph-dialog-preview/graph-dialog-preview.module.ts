import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GraphDialogPreviewComponent} from './graph-dialog-preview.component';
import {MatButtonToggleModule} from '@angular/material';

@NgModule({
    declarations: [GraphDialogPreviewComponent],
    imports: [CommonModule, MatButtonToggleModule],
})
export class GraphDialogPreviewModule {}
