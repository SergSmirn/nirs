import {ChangeDetectionStrategy, ChangeDetectorRef, Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';

enum info {
    successTitle = 'Готово',
    successMessage = 'Регистрация успешно завершена. Можете авторизоваться, используя форму входа',
    errorTitle = 'Ошибка',
    errorMessage = 'Ссылка не действительна, либо аккаунт уже активирован',
}

@Component({
    selector: 'app-activate-profile',
    templateUrl: './activate-profile.component.html',
    styleUrls: ['./activate-profile.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ActivateProfileComponent {
    message = '';
    title = '';

    constructor(
        private readonly router: Router,
        private readonly activatedRoute: ActivatedRoute,
        private readonly cd: ChangeDetectorRef,
        private readonly authService: AuthService,
    ) {
        const {uid, token} = this.activatedRoute.snapshot.params;

        this.authService.activate({uid, token}).subscribe(
            () => {
                this.title = info.successTitle;
                this.message = info.successMessage;
                this.cd.markForCheck();

                setTimeout(() => this.router.navigate([]), 5000);
            },
            () => {
                this.title = info.errorTitle;
                this.message = info.errorMessage;

                this.cd.markForCheck();
            },
        );
    }
}
