import json

from rest_framework import serializers
from logic.models import Experiment, Device
import pandas as pd
import numpy as np
from django.core.validators import FileExtensionValidator


class DeviceSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Device
        fields = ('id', 'user', 'name', 'process_node', 'supply_voltage', 'resistance', 'capacitance', 'producer',
                  'technology', 'comment')

    @staticmethod
    def validate_capacitance(value):
        return value * 1e-12


class BaseExperimentSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    status = serializers.SerializerMethodField()
    calculating_duration = serializers.SerializerMethodField()

    class Meta:
        model = Experiment

    @staticmethod
    def get_status(obj):
        if obj.started_at is None:
            status = 'in_queue'
        elif obj.finished_at is None:
            status = 'calculating'
        else:
            status = 'finished'
        return status

    def get_calculating_duration(self, obj):
        calculating_duration = None
        if self.get_status(obj) == 'finished':
            calculating_duration = obj.finished_at - obj.started_at
        return calculating_duration


class BaseCreateExperimentSerializer(BaseExperimentSerializer):
    spectre = serializers.FileField(validators=[FileExtensionValidator(allowed_extensions=['let', 'txt'])],
                                    write_only=True)

    experimental_data = serializers.CharField(write_only=True, required=False)

    experimental_data_file = serializers.FileField(
        validators=[FileExtensionValidator(allowed_extensions=['xls', 'xlsx', 'csv', 'txt', 'dat'])],
        write_only=True, required=False)

    def __init__(self, *args, **kwargs):
        super(BaseCreateExperimentSerializer, self).__init__(*args, **kwargs)
        if self.context['request'].method == 'POST':
            self.fields['device'] = serializers.PrimaryKeyRelatedField(required=True, queryset=Device.objects
                                                                       .filter(user=self.context['request'].user))

        elif self.context['request'].method == 'GET':
            self.fields['device'] = DeviceSerializer()

    def validate_spectre(self, value):
        try:
            self.initial_data['spectre_file_name'] = value.name
            self.initial_data['spectre_file_text'] = value.read()
            if value.name.split('.')[-1] == 'let':
                data_list = np.loadtxt(value, skiprows=23).tolist()
                data_list = list(map(lambda arr: [arr[0], arr[2]], data_list))
            else:
                data_list = np.loadtxt(value, skiprows=12).tolist()
                data_list = list(map(lambda arr: [arr[0], arr[1] / (60 * 60 * 24)], data_list))
        except:
            id = self.initial_data.get('id')
            if id is not None:
                exp = Experiment.objects.filter(pk=id).first()
                if exp:
                    return exp.spectre
            raise serializers.ValidationError('Bad file')
        return data_list

    def validate_spectre_file_name(self, value):
        return value

    def validate_experimental_data_file(self, value):
        # todo handling csv and txt extensions
        try:
            self.initial_data['data_file_name'] = value.name
            if value.name.split('.')[-1] == 'dat':
                data = np.loadtxt(value).tolist()
                data_list = [[], []]

                for row in data:
                    data_list[0].append(row[0])
                    data_list[1].append(row[1])
            else:
                df = pd.read_excel(value)
                data_list = [list(df.as_matrix().T[0]), list(df.as_matrix().T[1])]
        except:
            id = self.initial_data.get('id')
            if id is not None:
                exp = Experiment.objects.filter(pk=id).first()
                if exp:
                    return exp.experimental_data
            raise serializers.ValidationError('Bad file')
        return data_list

    def validate_experimental_data(self, value):
        data = json.loads(value)
        data_list = [[], []]

        for row in data:
            data_list[0].append(row[0])
            data_list[1].append(row[1])
            
        return data_list

    def validate(self, attrs):
        if 'experimental_data_file' in attrs:
            attrs['experimental_data'] = attrs.pop('experimental_data_file')
        return attrs


class ExperimentBaseAccessSerializer(BaseCreateExperimentSerializer):
    class Meta:
        model = Experiment
        fields = ('id', 'user', 'spectre', 'experimental_data', 'spectre_file_name', 'spectre_file_text',
                  'device', 'created_at', 'status', 'calculating_duration', 'experimental_data_file', 'data_file_name')

        read_only_fields = ('created_at', 'status', 'calculating_duration')
        extra_kwargs = {
            'spectre': {'write_only': True},
            'experimental_data': {'write_only': True}
        }


class ExperimentFullAccessSerializer(BaseCreateExperimentSerializer):
    class Meta:
        model = Experiment
        fields = (
            'id', 'user', 'spectre', 'experimental_data', 'spectre_file_name', 'spectre_file_text', 'device',
            'model_type',
            'sim_type', 'geometry', 'diff_coefficient',
            'ambipolar_diff_coefficient', 'trials_count', 'experimental_data_file',
            'particles_count', 'let_values_count', 'created_at', 'status', 'calculating_duration', 'data_file_name')

        read_only_fields = ('created_at', 'status', 'calculating_duration')
        extra_kwargs = {
            'spectre': {'write_only': True},
            'experimental_data': {'write_only': True},
            'model_type': {'write_only': True},
            'sim_type': {'write_only': True},
            'geometry': {'write_only': True},
            'diff_coefficient': {'write_only': True},
            'ambipolar_diff_coefficient': {'write_only': True},
            'trials_count': {'write_only': True},
            'particles_count': {'write_only': True},
            'let_values_count': {'write_only': True}
        }


class ExperimentDetailSerializer(BaseExperimentSerializer):
    device = DeviceSerializer()

    class Meta:
        model = Experiment
        fields = (
            'id', 'user', 'spectre', 'spectre_file_name', 'spectre_file_text', 'experimental_data', 'data_file_name',
            'device', 'par1', 'par2', 'model_type', 'sim_type', 'geometry', 'diff_coefficient',
            'ambipolar_diff_coefficient', 'trials_count', 'particles_count', 'let_values_count', 'cross_section', 'ser',
            'status', 'calculating_duration')

    def to_representation(self, instance):
        ret = super(ExperimentDetailSerializer, self).to_representation(instance)

        if instance.ser is not None:
            ret['ser'] = '{:0.2e}'.format(instance.ser * 3600 * 24)
            ret['par1'] = '{:0.2e}'.format(instance.par1)
            ret['par2'] = '{:0.2e}'.format(instance.par2)

        return ret
