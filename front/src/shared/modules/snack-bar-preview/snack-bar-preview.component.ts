import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {MAT_SNACK_BAR_DATA, MatDialog, MatSnackBarRef} from '@angular/material';
import {ResultDialogPreviewComponent} from '../../../modules/results/result-dialog-preview/result-dialog-preview.component';

type StatusType = 'error' | 'info';

interface IPreviewData {
    status: StatusType;
    message?: string;
    closer?: boolean;
    id?: number;
    experimentFinished: boolean;
}

const DEFAULT_CONFIG: IPreviewData = {
    status: 'error',
    closer: false,
    message: 'Попробуйте еще раз',
    experimentFinished: false,
};

@Component({
    selector: 'app-snack-bar-preview',
    templateUrl: './snack-bar-preview.component.html',
    styleUrls: ['./snack-bar-preview.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SnackBarPreviewComponent {
    readonly content: IPreviewData;

    constructor(
        private readonly snackBarRef: MatSnackBarRef<SnackBarPreviewComponent>,
        private readonly dialog: MatDialog,
        @Inject(MAT_SNACK_BAR_DATA) private readonly data: IPreviewData,
    ) {
        this.content = {...DEFAULT_CONFIG, ...data};
    }

    close() {
        this.snackBarRef.dismiss();
    }

    showResult() {
        this.dialog.open(ResultDialogPreviewComponent, {
            data: {id: this.content.id},
        });
        this.close();
    }
}
