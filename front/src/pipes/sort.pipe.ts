import {Pipe, PipeTransform} from '@angular/core';
import {IExperiment} from '../models/experiments.model';

export interface ISortArgs {
    order: boolean;
    field: string;
}

function dateUpOrder(field: string, a: IExperiment, b: IExperiment): number {
    return <any>new Date(a[field]) - <any>new Date(b[field]);
}

function dateDownOrder(field: string, a: IExperiment, b: IExperiment): number {
    return <any>new Date(b[field]) - <any>new Date(a[field]);
}

function defaultSort(
    order: boolean,
    field: string,
    a: IExperiment,
    b: IExperiment,
): number {
    if (a[field] < b[field]) {
        return order ? -1 : 1;
    }

    if (a[field] > b[field]) {
        return order ? 1 : -1;
    }

    return 0;
}

function sortByDevice(order: boolean, a: IExperiment, b: IExperiment): number {
    if (a.device.name < b.device.name) {
        return order ? -1 : 1;
    }

    if (a.device.name > b.device.name) {
        return order ? 1 : -1;
    }

    return 0;
}

@Pipe({
    name: 'sort',
})
export class SortPipe implements PipeTransform {
    transform(results: IExperiment[], {order, field}: ISortArgs): IExperiment[] {
        if (!results) {
            return;
        }

        if (field === 'createdExperiment') {
            const sort = order
                ? dateUpOrder.bind(null, field)
                : dateDownOrder.bind(null, field);

            return results.sort(sort);
        }

        const sortFunc =
            field === 'device'
                ? sortByDevice.bind(null, order)
                : defaultSort.bind(null, order, field);

        return results.sort(sortFunc);
    }
}
