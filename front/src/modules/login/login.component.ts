import {
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    Component,
    ViewChild,
} from '@angular/core';
import {
    AbstractControl,
    FormBuilder,
    FormGroup,
    FormGroupDirective,
    Validators,
} from '@angular/forms';
import {getErrors} from '../../utils/formValidators';
import {AuthService} from '../../services/auth.service';
import {IUser} from '../../models/user.model';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material';
import {SnackBarPreviewComponent} from '../../shared/modules/snack-bar-preview/snack-bar-preview.component';

const DEFAULT_MESSAGE = 'Попробуйте еще раз позже';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.less'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginComponent {
    form: FormGroup;
    getErrors = getErrors;
    showLoader = false;

    @ViewChild(FormGroupDirective) private _form;

    constructor(
        private readonly fb: FormBuilder,
        private readonly cd: ChangeDetectorRef,
        private readonly router: Router,
        private readonly authService: AuthService,
        private readonly snackBar: MatSnackBar,
    ) {
        this.form = this.fb.group({
            username: ['', [Validators.required]],
            password: ['', [Validators.required]],
        });
    }

    get username(): AbstractControl {
        return this.form.get('username');
    }

    get password(): AbstractControl {
        return this.form.get('password');
    }

    onSubmit() {
        Object.keys(this.form.controls).forEach(key => {
            this.form.get(key).markAsTouched();
        });

        if (this.form.invalid) {
            return;
        }

        const user = <IUser>{
            username: this.username.value,
            password: this.password.value,
        };

        this.showLoader = !this.showLoader;

        this.authService.login(user).subscribe(
            () => {},
            ({status}) => {
                const message = this.getMessage(status);

                this.snackBar.openFromComponent(SnackBarPreviewComponent, {
                    verticalPosition: 'top',
                    horizontalPosition: 'right',
                    duration: 5000,
                    data: {message, closer: true},
                });

                this.showLoader = !this.showLoader;
                this.cd.markForCheck();
            },
        );
    }

    private getMessage(status: number): string {
        if (status === 400) {
            return 'Неправильный логин или пароль';
        }

        if (status === 406) {
            return (
                'Аккаунт не активирован. ' +
                'Вам необходимо перейти по ссылке в письме на вашей почте.'
            );
        }

        return DEFAULT_MESSAGE;
    }
}
