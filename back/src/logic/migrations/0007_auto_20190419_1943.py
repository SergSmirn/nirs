# Generated by Django 2.1.2 on 2019-04-19 19:43

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('logic', '0006_experiment_cross_section'),
    ]

    operations = [
        migrations.AlterField(
            model_name='experiment',
            name='cross_section',
            field=django.contrib.postgres.fields.ArrayField(base_field=django.contrib.postgres.fields.ArrayField(base_field=models.FloatField(), size=None), null=True, size=None),
        ),
    ]
