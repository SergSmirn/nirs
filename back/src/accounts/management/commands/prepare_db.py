from django.core.management.base import BaseCommand
from django.contrib.auth.models import Group
from django.contrib.auth import get_user_model


User = get_user_model()


class Command(BaseCommand):
    help = 'Insert initial data to db'

    def handle(self, *args, **options):
        if not Group.objects.all().exists():
            Group.objects.create(name='base_access')
            full_group = Group.objects.create(name='full_access')
            user = User.objects.create_superuser('serg', 'q@q.ru', 'q')
            user.groups.add(full_group)
            self.stdout.write(self.style.SUCCESS('All successfully created'))
        else:
            self.stdout.write(self.style.SUCCESS('All already created'))
