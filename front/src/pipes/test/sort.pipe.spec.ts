import {SortPipe} from '../sort.pipe';

const mockExperiments = [
    {
        device: {
            name: 'a',
        },
        createdExperiment: '11.12.2018, 12:12',
        status: 'a',
    },
    {
        device: {
            name: 'b',
        },
        createdExperiment: '12.12.2018, 12:12',
        status: 'b',
    },
    {
        device: {
            name: 'c',
        },
        createdExperiment: '13.12.2018, 12:12',
        status: 'c',
    },
];
describe('SortPipe', () => {
    let pipe;

    beforeEach(() => {
        pipe = new SortPipe();
    });

    it('create an instance', () => {
        expect(pipe).toBeTruthy();
    });

    describe('По возрастанию ', () => {
        const order = true;
        const experiments = [...mockExperiments];

        it('device name', () => {
            const field = 'device';
            const sortBy = {field, order};
            const sort = pipe.transform(experiments, sortBy);
            const mock = [mockExperiments[0], mockExperiments[1], mockExperiments[2]];

            expect(sort).toEqual(mock);
        });

        it('createdExperiment', () => {
            const field = 'createdExperiment';
            const sortBy = {field, order};
            const sort = pipe.transform(experiments, sortBy);
            const mock = [mockExperiments[0], mockExperiments[1], mockExperiments[2]];

            expect(sort).toEqual(mock);
        });

        it('status', () => {
            const field = 'status';
            const sortBy = {field, order};
            const sort = pipe.transform(experiments, sortBy);
            const mock = [mockExperiments[0], mockExperiments[1], mockExperiments[2]];

            expect(sort).toEqual(mock);
        });
    });

    describe('По убыванию ', () => {
        const order = false;
        const experiments = [...mockExperiments];

        it('device name', () => {
            const field = 'device';
            const sortBy = {field, order};
            const sort = pipe.transform(experiments, sortBy);
            const mock = [mockExperiments[2], mockExperiments[1], mockExperiments[0]];

            expect(sort).toEqual(mock);
        });

        it('createdExperiment', () => {
            const field = 'createdExperiment';
            const sortBy = {field, order};
            const sort = pipe.transform(experiments, sortBy);
            const mock = [mockExperiments[2], mockExperiments[1], mockExperiments[0]];

            expect(sort).toEqual(mock);
        });

        it('status', () => {
            const field = 'status';
            const sortBy = {field, order};
            const sort = pipe.transform(experiments, sortBy);
            const mock = [mockExperiments[2], mockExperiments[1], mockExperiments[0]];

            expect(sort).toEqual(mock);
        });
    });
});
