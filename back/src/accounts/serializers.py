from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from accounts.models import User
import jwt


class UserCreateSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    username = serializers.CharField(
        validators=[UniqueValidator(queryset=User.objects.all())]
    )
    password = serializers.CharField(min_length=8, write_only=True)

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password')


class UserShortSerializer(serializers.ModelSerializer):
    group = serializers.SerializerMethodField()
    token = serializers.SerializerMethodField()
    has_running_experiments = serializers.SerializerMethodField()

    @staticmethod
    def get_group(obj):
        return obj.groups.first().name

    @staticmethod
    def get_token(obj):
        return jwt.encode({"sub": str(obj.pk)}, "secret").decode()

    @staticmethod
    def get_has_running_experiments(obj):
        return obj.experiment_set.filter(finished_at=None).exists()

    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name', 'user_avatar', 'group', 'token', 'has_running_experiments')
