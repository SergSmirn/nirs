import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {MatSnackBar} from '@angular/material';
import {ExperimentsService} from './experiments.service';
import {SnackBarPreviewComponent} from '../shared/modules/snack-bar-preview/snack-bar-preview.component';
import {IExperiment, IExperimentStatus} from '../models/experiments.model';

const NOT_SHOW_NOTIFICATION_URLS = ['/results'];

@Injectable({
    providedIn: 'root',
})
export class StatusHandlerService {
    constructor(
        private readonly experimentsService: ExperimentsService,
        private readonly snackBar: MatSnackBar,
        private readonly router: Router,
    ) {}

    initialize() {
        this.experimentsService.socketInfo$.subscribe(this.statusHandler.bind(this));
    }

    private statusHandler(data: IExperimentStatus) {
        const currentExperiment = this.experimentsService._experiments.find(
            ({id: expId}) => expId === data.id,
        );
        const updatedExp = {...currentExperiment, ...data};
        const updatedExps = this.experimentsService._experiments.map(experiment => {
            return experiment.id === data.id ? updatedExp : experiment;
        });

        this.experimentsService._experiments = updatedExps;
        this.experimentsService._experiments$.next(updatedExps);

        const {status, id} = updatedExp;
        const inCache = this.experimentsService.cachedExperiments.get(id);
        const experimentFinished = status === 'finished';
        let duration;

        if (experimentFinished && !!inCache) {
            this.experimentsService.getExperimentResult(id, true);
        }

        if (!experimentFinished) {
            duration = {duration: 5000};
        }

        if (NOT_SHOW_NOTIFICATION_URLS.indexOf(this.router.url) !== -1) {
            return;
        }

        const message = this.getMessage(updatedExp);

        this.snackBar.openFromComponent(SnackBarPreviewComponent, {
            verticalPosition: 'top',
            horizontalPosition: 'right',
            ...duration,
            data: {message, status: 'info', closer: true, experimentFinished, id},
        });
    }

    private getMessage({status, progress}: IExperiment): string {
        switch (status) {
            case 'in_queue':
                return 'Эксперимент в очереди';
            case 'started':
                return 'Эксперимент начал свой расчет';
            case 'calculating':
                return `Эксперимент выполнен на: ${progress}%`;
            case 'finished':
                return 'Эксперимент посчитан';
        }
    }
}
