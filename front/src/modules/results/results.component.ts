import {ChangeDetectionStrategy, Component} from '@angular/core';
import {animate, style, transition, trigger} from '@angular/animations';
import {Observable} from 'rxjs';
import {IExperiment} from '../../models/experiments.model';
import {ExperimentsService} from '../../services/experiments.service';
import {ISortArgs} from '../../pipes/sort.pipe';

@Component({
    selector: 'app-results',
    templateUrl: './results.component.html',
    styleUrls: ['./results.component.less'],
    animations: [
        trigger('animate', [
            transition(':enter', [
                style({height: 0, opacity: 0}),
                animate('300ms ease', style({height: '*', opacity: 1})),
            ]),
            transition(':leave', [
                style({height: '*', opacity: 1}),
                animate('300ms ease', style({height: 0, opacity: 0})),
            ]),
        ]),
    ],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResultsComponent {
    rows = new Array(100);
    openIndexes: number[] = [];
    experiments$: Observable<IExperiment[]>;
    sortField = 'createdExperiment';
    order = false;

    constructor(private readonly experimentsService: ExperimentsService) {
        this.experimentsService.updateExperimentsStorage();
        this.experiments$ = this.experimentsService.experiments$;
    }

    get getSortBy(): ISortArgs {
        return {field: this.sortField, order: this.order};
    }

    onClick(id: number) {
        if (this.openIndexes.includes(id)) {
            this.openIndexes = this.openIndexes.filter(index => index !== id);
        } else {
            this.openIndexes.push(id);
            this.experimentsService.getExperimentResult(id);
        }
    }

    sortBy(field: string) {
        if (this.sortField === field) {
            this.order = !this.order;

            return;
        }

        this.sortField = field;
    }

    isCurrentSortField(field: string): boolean {
        return this.sortField === field;
    }

    getArrowUp(field: string): boolean {
        return this.sortField === field && !this.order;
    }
}
