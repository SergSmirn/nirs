import {ChangeDetectionStrategy, Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material';
import * as canvasJS from '../../../assets/canvasjs.min.js';

enum Axis {
    DEFAULT = 'default',
    LOG = 'log',
    LN = 'ln',
}

const noopUpdate = (x: number) => x;

function getUpdateFunc(type: Axis, axisName: 'axisX' | 'axisY'): (v: number) => number {
    switch (type) {
        case Axis.LOG:
            return Math.log10;
        case Axis.LN:
            return toLn;
        default:
            return axisName === 'axisY' ? (v: number) => v * 10e7 : noopUpdate;
    }
}

function toLn(x: number): number {
    return Math.log10(x) / Math.log10(Math.E);
}

function getMinFinitNumber(numbers: number[]): number {
    let number = -Infinity;

    numbers.forEach(num => {
        number = Number.isFinite(number) ? Math.min(number, num) : num;
    });

    return number === -Infinity ? Number.MIN_SAFE_INTEGER : number;
}

function getMaxFinitNumber(numbers: number[]): number {
    let number = +Infinity;

    numbers.forEach(num => {
        number = Number.isFinite(number) ? Math.max(number, num) : num;
    });

    return number === +Infinity ? Number.MAX_SAFE_INTEGER : number;
}

function getTitle(type: Axis, str: string): string {
    switch (type) {
        case Axis.LN:
            return `ln( ${str} )`;
        case Axis.LOG:
            return `log( ${str} )`;
        default:
            return str;
    }
}

@Component({
    selector: 'app-graph-dialog-preview',
    templateUrl: './graph-dialog-preview.component.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GraphDialogPreviewComponent implements OnInit {
    xAxisPreview = Axis.DEFAULT;
    yAxisPreview = Axis.LOG;

    axisOptions = Object.values(Axis);

    private readonly simulationResult: {x: number; y: number}[];
    private readonly experimentalData: {x: number; y: number}[];
    private readonly experimentalDataFile: string;
    private chart: any;

    constructor(@Inject(MAT_DIALOG_DATA) private data: any) {
        const simulationResultX = this.data.simulationResult[0];
        const experimentalDataX = this.data.experimentalData[0];

        this.experimentalDataFile = this.data.experimentalDataFile;
        this.simulationResult = simulationResultX.map((x, i) => {
            const y = this.data.simulationResult[1][i];

            return {x, y: y};
        });
        this.experimentalData = experimentalDataX.map((x, i) => {
            const y = this.data.experimentalData[1][i];

            return {x, y: y};
        });
    }

    ngOnInit() {
        const updateX = getUpdateFunc(this.xAxisPreview, 'axisX');
        const updateY = getUpdateFunc(this.yAxisPreview, 'axisY');
        const {simulationResult, experimentalData} = this.updateChartData(
            updateX,
            updateY,
        );

        this.rendererChart(simulationResult, experimentalData);
    }

    changeAxisPreview({value: axisType}: any, axisName: 'axisX' | 'axisY') {
        if (axisName === 'axisX') {
            this.xAxisPreview = axisType;
        } else {
            this.yAxisPreview = axisType;
        }

        const updateX = getUpdateFunc(this.xAxisPreview, 'axisX');
        const updateY = getUpdateFunc(this.yAxisPreview, 'axisY');
        const {simulationResult, experimentalData} = this.updateChartData(
            updateX,
            updateY,
        );

        this.rendererChart(simulationResult, experimentalData);
    }

    private toggleDataSeries(e) {
        if (!e) {
            return;
        }

        e.dataSeries.visible =
            typeof e.dataSeries.visible !== 'undefined' && !e.dataSeries.visible;
        this.chart.render();
    }

    private updateChartData(
        xUpdate: (x: number) => number,
        yUpdate: (y: number) => number,
    ): {
        simulationResult: {x: number; y: number}[];
        experimentalData: {x: number; y: number}[];
    } {
        const simulationResult = this.simulationResult.map(({x, y}) => {
            return {
                x: xUpdate(x),
                y: yUpdate(y),
            };
        });

        const experimentalData = this.experimentalData.map(({x, y}) => {
            return {
                x: xUpdate(x),
                y: yUpdate(y),
            };
        });

        return {
            simulationResult,
            experimentalData,
        };
    }

    private rendererChart(
        simulationResult: {x: number; y: number}[],
        experimentalData: {x: number; y: number}[],
    ) {
        const xArray = [];
        const yArray = [];

        [...simulationResult, ...experimentalData].forEach(({x, y}) => {
            xArray.push(x);
            yArray.push(y);
        });

        const minX = getMinFinitNumber(xArray);
        const maxX = getMaxFinitNumber(xArray);
        const minY = getMinFinitNumber(yArray);
        const maxY = getMaxFinitNumber(yArray);

        const xTitle = getTitle(this.xAxisPreview, 'ЛПЭ, МэВ/см2∙мг');
        const yTitle = getTitle(this.yAxisPreview, 'Сечение, см2 * 10e-7');

        this.chart = new canvasJS.Chart('chartContainer', {
            zoomEnabled: true,
            axisY: {
                logarithmic: false,
                title: yTitle,
                minimum: minY,
                maximum: maxY,
            },
            axisX: {
                title: xTitle,
                minimum: minX,
                maximum: maxX,
            },
            toolTip: {
                enabled: true,
                content: '{name}<br>ЛПЭ: {x}<br>Сечение: {y}',
            },
            legend: {
                cursor: 'pointer',
                verticalAlign: 'top',
                dockInsidePlotArea: true,
                horizontalAlign: 'left',
                itemclick: this.toggleDataSeries.bind(this),
            },
            data: [
                {
                    type: 'line',
                    showInLegend: true,
                    markerSize: 0,
                    name: 'Результат эксперимента',
                    dataPoints: simulationResult,
                },
                {
                    type: 'scatter',
                    showInLegend: true,
                    name: `Данные ${this.experimentalDataFile}`,
                    dataPoints: experimentalData,
                },
            ],
        });

        this.chart.render();
    }
}
