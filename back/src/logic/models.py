from django.db import models
from accounts.models import User
from django.contrib.postgres.fields import ArrayField


class Device(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=1024)
    process_node = models.IntegerField(default=200)
    supply_voltage = models.FloatField(default=8)
    resistance = models.FloatField(default=1.5e4)
    capacitance = models.FloatField(default=1e-15)
    producer = models.CharField(max_length=1024, blank=True, null=True)
    technology = models.CharField(max_length=1024, blank=True, null=True)
    comment = models.CharField(max_length=1024, blank=True, null=True)

    def __str__(self):
        return '{0}\nnode: {1}\nvoltage: {2}\nresistance: {3}\ncapacitance: {4}'.format(self.name, self.process_node,
                                                                                        self.supply_voltage,
                                                                                        self.resistance,
                                                                                        self.capacitance)


class Experiment(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    started_at = models.DateTimeField(null=True)
    finished_at = models.DateTimeField(null=True)

    # device
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    device = models.ForeignKey(Device, default=None, on_delete=models.CASCADE, blank=True)

    # models
    par1 = models.FloatField(null=True)
    par2 = models.FloatField(null=True)
    cross_section = ArrayField(ArrayField(models.FloatField()), null=True)
    model_type = models.CharField(max_length=10, choices=(('point', 'Точечная модель'),
                                                          ('diffusion', 'Диффузионная модель'),
                                                          ('thin', 'Тонкий слой')),
                                  default='diffusion')

    sim_type = models.CharField(max_length=15, choices=(('monte_carlo', 'Монте-Карло'),
                                                        ('analytical', 'Аналитический')),
                                default='analytical')

    geometry = models.CharField(max_length=15, choices=(('disc', 'Диск'),
                                                        ('sphere', 'Сфера')),
                                default='disc')

    spectre = ArrayField(ArrayField(models.FloatField()))
    spectre_file_name = models.CharField(max_length=1024, null=True)
    spectre_file_text = models.TextField(null=True)
    experimental_data = ArrayField(ArrayField(models.FloatField()), size=2)
    data_file_name = models.CharField(max_length=1024, null=True)

    # phys
    diff_coefficient = models.FloatField(default=12.)
    ambipolar_diff_coefficient = models.FloatField(default=25.)

    # accuracy
    trials_count = models.IntegerField(default=20)
    particles_count = models.IntegerField(default=50000)
    let_values_count = models.IntegerField(default=300)

    # let-cross_section
    simulation_result = ArrayField(ArrayField(models.FloatField()), null=True)

    ser = models.FloatField(null=True)
