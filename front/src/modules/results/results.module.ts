import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MatIconModule, MatProgressBarModule} from '@angular/material';
import {ResultsComponent} from './results.component';
import {ResultsRoutingModule} from './resultsRouting.module';
import {ResultsPreviewModule} from './results-preview/results-preview.module';
import {ToDatePipe} from '../../pipes/to-date.pipe';
import {ExperimentStatePipe} from '../../pipes/experiment-state.pipe';
import {SortPipe} from '../../pipes/sort.pipe';

@NgModule({
    declarations: [ResultsComponent, ToDatePipe, ExperimentStatePipe, SortPipe],
    imports: [
        CommonModule,
        ResultsRoutingModule,
        ResultsPreviewModule,
        MatIconModule,
        MatProgressBarModule,
    ],
})
export class ResultsModule {}
